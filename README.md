# Multiple Azure Kinect GUI

Multiple Azure Kinect GUI is an MFC-based GUI application for the control of multiple (or a single) Azure Kinect cameras connected to multiple (or a single) working PC

## Software Description

This software is a wrapper of the Azure Kinect SDK and Azure Kinect Body Tracking SDK. 
Its main features are:

- Handling of multiple cameras:
    - on a single PC, depending on how many cameras are connected. External sync options are STANDALONE and MASTER-SLAVE (if trigger cables are connected)
    - on multiple PCs. External sync opitons are STANDALONE and MASTER-SLAVE with a local master on the current PC or a remote master on another PC (acquisition will not start before the master grabs the first frame) 
- Graphical user interface to manage cameras and options
- Body Tracking enabled or disabled depending on the aims
- Data recording:
    - Single multimodal pictures 
    - Matroska (.mkv) videos, as in the documentation of Azure Kinect SDK
- Matroska video playback:
    - If multiple videos are recorded from different cameras, the playback can load them simultaneously (please, maintain the filename structure to make thinks working!)
    - It is possible to export data from a set of matroska videos:
        - Color images and color images aligned in the infrared geometry (system of coordinates and resolution)
        - Infrared images and infrared images aligned in the color geometry (system of coordinates and resolution)
        - Depth maps and depth maps aligned in the color geometry (system of coordinates and resolution)
        - Point clouds in the color or infrared geometry
        - User skeletons

_Graphical User Interface_
![alt text](Pictures/GUI.png) 

_Sample captures: Color, Infrared, Depth_
![alt text](Pictures/captures.png) 

_Body Tracking_
![alt text](Pictures/bodytracking.png) 


## System requirements
This module works under the Microsoft Windows 10 and 11 OS.

Please check the other system requirements at the [Azure Kinect](https://docs.microsoft.com/en-us/azure/Kinect-dk/system-requirements) corresponding web page.

Tested with Azure Kinect Body Tracking SDK at version 1.1.2.

## Installation
This is the list of instructions for the installation of the Multiple Azure Kinect GUI from released setup:

1. Install all the drivers of the Azure Kinect Sensor following the quickstart guide available [here](https://docs.microsoft.com/en-us/azure/kinect-dk/set-up-azure-kinect-dk).
2. Install all the drivers of the Azure Kinect Body Tracking library following the quickstart guide available [here](https://docs.microsoft.com/en-us/azure/kinect-dk/body-sdk-setup).
3. Add the path of the _tools folder_ of the Azure Kinect Body Tracking directory (e.g. C:\Program Files\Azure Kinect Body Tracking SDK\tools) to the environmental variable PATH of the system.
4. Install the software release available [here](Release/Setup.msi)

Enjoy!

In case of problems with missing dlls, please download Microsoft Visual C++ Redistributable packages for Visual Studio 2015, 2017, 2019, and 2022, available [here](https://learn.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist?view=msvc-170) (e.g. [vc_redist.x64.exe](https://aka.ms/vs/17/release/vc_redist.x64.exe))

## To do list
- Add IMU export
- Add further resolution settings

## Contacts
For further information, please send a mail to Roberto Marani (roberto.marani@stiima.cnr.it)

## License
[MIT](https://choosealicense.com/licenses/mit/) - 2022
