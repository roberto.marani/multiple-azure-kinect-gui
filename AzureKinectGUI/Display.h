#pragma once
#include "AzureKinect.h"
#include <inttypes.h>
#include <opencv2/core/ocl.hpp>

class CDisplay
{
public:
	CDisplay();
	~CDisplay();

protected:
	bool bWinOpen;
	cv::String m_winName1, m_winName2;
	int CDisplay::DrawBodyOnColorImage(int idxBodyFrame, cv::Mat &Image);
	template <class T> int FindCorrespondingIdx(std::vector<T> src, std::string serialNumber);

	class CCountWindows
	{
	public:
		int CountThem();
		std::vector<HWND> hWindows;
		std::vector<CString> WinNames;
	private:
		static BOOL CALLBACK StaticWndEnumProc(HWND hwnd, LPARAM lParam);
		BOOL WndEnumProc(HWND hwnd);
		int m_count;
	};

public:

	struct ColorUChar {
		uchar r = 255;
		uchar g = 255;
		uchar b = 255;
	};

	const std::array<ColorUChar, 20> g_bodyColors =
	{
		0, 255, 255,
		255, 166, 0,
		150, 255, 125,
		0, 127, 127,
		216, 112, 216,
		0, 255, 127,
		135, 214, 252,
		255, 105, 71,
		20, 178, 160,
		217, 158, 217,
		0, 252, 153,
		0, 255, 185,
		135, 214, 252,
		0, 127, 127,
		216, 112, 216,
		0, 255, 127,
		255, 105, 71,
		20, 178, 160,
		217, 158, 217,
		0, 252, 153,
	};

public:

	std::vector<AK::SImage> Colors, IRs, Depths, BodyIndexMapsA;
	std::vector<AK::SSkeleton> Skeletons;
	std::vector<AK::SIMU> IMUs;
	std::vector<AK::SBodyID> BodyIDs;

	std::vector<AK::SPointcloud> PointcloudsColor, PointcloudsDepth;
	std::vector<AK::SImage> ColorsA, DepthsA, IRsA;

	void *p_Container;

	size_t nImageMax;
	std::string captureDestinationFolder;
	int iSaveFrame;

	void Init(bool withBodyTracking);
	void ReInit(bool withBodyTracking);
	HWND GetWindowHandle(std::string inputWinName);
	void SetContainer(void* Container);
	void Close(bool withBodyTracking);
	void ClearVariables();

	void ShowCaptures();
	void ShowBodyTracking();

	void SaveCaptures();
	
	void SendImuDataToDlg();
	void AppendImuDataToDlg(LPCTSTR Message);
	void EmptyImuDataInDlg();

};

template<class T>
inline int CDisplay::FindCorrespondingIdx(std::vector<T> src, std::string serialNumber)
{
	for (int i = 0; i < src.size(); i++) {
		if (src[i].SerialNumber == serialNumber)
			return i;
	}
	return -1;
}
