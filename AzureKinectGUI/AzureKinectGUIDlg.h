
// AzureKinectGUI.h : header file
//

#pragma once
#include "Controller.h"
#include "OutputDlg.h"
#include "Player.h"

// CAzureKinectDlg dialog
class CAzureKinectDlg : public CDialogEx
{
// Construction
public:
	CAzureKinectDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_AZUREKINECTGUI_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();

	CController Controller;
	CPlayer Player;
	CButton m_radioAutoExp, m_radioManualExp, m_radioStandalone, m_radioLocalmaster, m_radioRemotemaster, m_doBodyTracking;
	CEdit m_editExp, m_editImu, m_editLog;
	CComboBox m_comboConfig;
	CString m_SnapshotPath;

public:
	afx_msg void OnBnClickedStartcapture();
	afx_msg void OnBnClickedStopcapture();
	afx_msg void OnCbnSelchangeComboconfig();
	afx_msg void OnBnClickedAutoexp();
	afx_msg void OnBnClickedManualexp();
	afx_msg void OnEnChangeEditexp();
	afx_msg void OnBnClickedStartrecord();
	afx_msg void OnBnClickedStoprecord();
	const std::string GetCurrentDateTime();
	afx_msg void OnBnClickedSetreferencefile();
	CEdit m_editPlayerFilePathAndName;
	CSliderCtrl m_slider;
	afx_msg void OnBnClickedCloseplayer();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	CButton m_checkSkeletons, m_checkPointcloudsColor, m_checkPointcloudsDepth, m_checkColor, m_checkColorAligned, m_checkDepth, m_checkDepthAligned, m_checkIr, m_checkIrAligned;
	afx_msg void OnBnClickedStartexport();
	afx_msg void OnBnClickedCheckskeletons();
	CProgressCtrl m_progressExport;
	afx_msg void OnBnClickedStopexport();
	CEdit m_editFrameIndex;
	afx_msg void OnBnClickedCheckpointcloudcolor();
	afx_msg void OnBnClickedCheckpointclouddepth();
	afx_msg void OnBnClickedLocalmaster();
	afx_msg void OnBnClickedRemotemaster();
	afx_msg void OnBnClickedStandalone();
	afx_msg void OnBnClickedCheckcolor();
	afx_msg void OnBnClickedCheckdepthaligned();
	afx_msg void OnBnClickedCheckiraligned();
	afx_msg void OnBnClickedCheckcoloraligned();
	afx_msg void OnBnClickedCheckdepth();
	afx_msg void OnBnClickedCheckir();
	afx_msg void OnBnClickedCheckdobodytracking();
	afx_msg void OnBnClickedChange();
	afx_msg void OnBnClickedTake();
};
