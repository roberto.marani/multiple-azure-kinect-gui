#pragma once

#include <k4a/k4a.hpp>
#include <k4abt.hpp>
#include <k4arecord/record.hpp>
#include <k4arecord/playback.hpp>
#include <vector>
#include <string>
#include <chrono>
#include <opencv2/opencv.hpp>


namespace AK
{
	struct _timestamp {
		std::chrono::microseconds Device;
		std::chrono::nanoseconds System;
	};

	struct _PointXYZBGR {
		float x, y, z;
		uint8_t b, g, r;
	};

	const int MIN_RANGE = 300;
	const int MAX_RANGE = 3860;

	typedef enum _input_config {
		COLOR43_2048x1536_DEPTHN_640x576_30FPS,
		COLOR43_4096x3072_DEPTHN_640x576_15FPS,
		COLOR43_2048x1536_DEPTHW_512x512_30FPS,
		COLOR43_4096x3072_DEPTHW_1024x1024_15FPS,
		COLOR169_QHD_DEPTHN_640x576_30FPS,
		COLOR169_4K_DEPTHN_640x576_15FPS,
		COLOR169_QHD_DEPTHW_512x512_30FPS,
		COLOR169_4K_DEPTHW_1024x1024_15FPS,
		COLOR169_FHD_DEPTHN_640x576_30FPS,
		COLOR169_720P_DEPTHN_640x576_30FPS
	} EInputConfig;

	typedef enum _image_data {
		COLOR,
		COLORA,
		IR,
		IRA,
		DEPTH,
		DEPTHA, 
		SKELETON
	} EImageData;

	typedef enum _bodyindex_data {
		BODYINDEXMAP,
		BODYINDEXMAPA
	} EBodyIndexData;

	typedef struct _SImage {
		int ReferenceKinect;
		std::string SerialNumber;
		_timestamp Timestamp;
		int Height, Width;
		std::vector<uint8_t> Data;
	} SImage;

	typedef struct _SSkeleton {
		int ReferenceKinect;
		std::string SerialNumber;
		_timestamp Timestamp;
		uint32_t BodyID;
		struct _joint2D {
			float x, y;
		} Joint2DColorSpace[32], Joint2DDepthSpace[32];
		struct _joint3D {
			float Position[3];
			float Orientation[4];
		} Joint3D[32];
		float Confidence[32];
	} SSkeleton;

	typedef struct _SPointcloud {
		int ReferenceKinect;
		std::string SerialNumber;
		_timestamp Timestamp;
		std::vector<_PointXYZBGR> Points;
	} SPointcloud;

	typedef struct _SBodyID {
		std::string SerialNumber;
		_timestamp Timestamp;
		std::map<uint8_t, uint32_t> BodyIndexToBodyID;
	} SBodyID;

	typedef struct _SIMU {
		std::string SerialNumber;
		float Temperature;
		struct _acc {
			uint64_t Timestamp;
			float X;
			float Y;
			float Z;
		} Accelerometer;
		struct _gyro {
			uint64_t Timestamp;
			float X;
			float Y;
			float Z;
		} Gyroscope;
	} SIMU;	

}


class CAzureKinect
{
public:
	CAzureKinect();
	~CAzureKinect();

protected:
	typedef struct _MyKinect {
		std::string SerialNumber;
		struct MyDevice {
			k4a::device Worker;
			k4a_device_configuration_t Configuration;
		} Device;
		k4a::calibration Calibration;
		k4a::transformation Transformation;
		struct MyRecorder {
			k4a::record Worker;
			char Filename[256];
			bool b_recordImu;
		} Recorder;
		struct MyPlayback {
			k4a::playback Worker;
			k4a_record_configuration_t Configuration;
			char Filename[256];
			int NFrames;
			int Fps;
			std::chrono::microseconds Duration;
		} Player;
		k4abt::tracker Tracker;
	} MyKinect;

	typedef struct _MyImuSample {
		std::string SerialNumber;
		k4a_imu_sample_t Imu;
	} MyImuSample;

	typedef struct _MyCapture {
		int ReferenceKinect;
		k4a::capture Capture;
	} MyCapture;

	typedef struct _MyBodyFrame {
		int ReferenceCapture;
		k4abt::frame BodyFrame;
	} MyBodyFrame;


protected:
	std::vector<std::shared_ptr<MyKinect>> m_kinects;
	std::vector<std::shared_ptr<MyCapture>> m_captures;
	std::vector< std::shared_ptr<MyBodyFrame>> m_bodyFrames;
	std::vector<MyImuSample> m_imuSamples;

protected:
	void CloseSingleKinect(int i);	
	int GetCapture(int step_or_pos, int idxDevice, MyCapture &myCapture);
	bool GetIMU(int idxDevice, AK::SIMU & IMU);
	bool AssignSkeletons(k4abt_skeleton_t k4aSkeleton, k4abt::frame bodyFrame, std::shared_ptr<MyKinect> pKinect, AK::SSkeleton &skeleton);
	bool AssignSkeletonsLight(k4abt_skeleton_t k4aSkeleton, k4abt::frame bodyFrame, std::shared_ptr<MyKinect> pKinect, AK::SSkeleton &skeleton);
	cv::Mat k4aImageToCvMat(k4a::image image, int CV_TYPE, bool BGR2BGRA = false);
	template <class T> T mode(std::vector<T> v);	
	
public:
	int Open();
	int Open(std::vector<std::string> PlaybackFilenames);
	int Close();
	size_t Count();
	
	int SetConfiguration();
	size_t StartAcquisitions();
	bool StartRecordings(std::string BaseName);
	void GetRecordingsNames(std::vector<std::string>& Names);
	void SetExposures(int inExp);
	int StopAcquisitions();
	int StopRecordings();

	bool WaitForFirstCapture();
	size_t GetCaptures(int step_or_pos);
	size_t GetIMUSamples(int step_or_pos);
	void DoBodyTracking();
	void ReleaseCaptures();
	void ReleaseIMUSamples();
	void ReleaseBodyFrames();

	std::vector<int> GetPlayerDurations();
	void GetSerialNumbers(std::vector<std::string>& Serials);
	void GetValidKinects(std::vector<bool>& ValidKinects);
	int GetImages(std::vector<AK::SImage> &Images, AK::EImageData ImageData);
	int GetBodyIndexMaps(std::vector<AK::SImage>& Images, AK::EBodyIndexData BodyIndexData);
	int GetBodyIDs(std::vector<AK::SBodyID>& BodyIDs);
	int GetSkeletons(std::vector<AK::SSkeleton> &Skeletons, bool isLight = false);
	int GetPointcloudsOnColorGeometry(std::vector<AK::SPointcloud>& Clouds);
	int GetPointcloudsOnDepthGeometry(std::vector<AK::SPointcloud>& Clouds);
	int GetIMUs(std::vector<AK::SIMU> &IMUs);
		
	void AssignSkeletonsToParentKinect(std::vector<AK::SSkeleton> inSkeletons, std::vector<std::vector<AK::SSkeleton>>& perKinectSkeletons);

public:
	AK::EInputConfig m_inputConfig;	
	int i_extSync;
	int m_fps;	

public:
	void* p_Container;
	void SetContainer(void* Container);
	const wchar_t* GetWChar(const char* c);
	void AppendLogMessages(LPCTSTR Message);
	void EmptyLogMessages();

};

template<class T>
inline T CAzureKinect::mode(std::vector<T> v)
{
	T mode;
	// Sort elements to compute the mode
	sort(v.begin(), v.end());
	T curr = v[0];
	mode = curr;
	T count = 1;
	T countMode = 1;
	for (int i = 1; i < v.size(); i++) {
		if (v[i] == curr) // count occurrences of the current Body ID
			++count;
		else {	// now this is a different number
			if (count > countMode) {
				countMode = count; // mode is the biggest ocurrences
				mode = curr;
			}
			count = 1; // reset count for the new Body ID
			curr = v[i];
		}
	}
	if (count > countMode)
		mode = curr;
	return mode;
}
