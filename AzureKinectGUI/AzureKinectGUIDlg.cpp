
// AzureKinectGUIDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AzureKinectGUI.h"
#include "AzureKinectGUIDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CAzureKinectDlg dialog



CAzureKinectDlg::CAzureKinectDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_AZUREKINECTGUI_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CAzureKinectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_COMBOCONFIG, m_comboConfig);

	DDX_Control(pDX, IDC_AUTOEXP, m_radioAutoExp);
	DDX_Control(pDX, IDC_MANUALEXP, m_radioManualExp);
	DDX_Control(pDX, IDC_LOCALMASTER, m_radioLocalmaster);
	DDX_Control(pDX, IDC_REMOTEMASTER, m_radioRemotemaster);
	DDX_Control(pDX, IDC_STANDALONE, m_radioStandalone);
	DDX_Control(pDX, IDC_EDITEXP, m_editExp);
	DDX_Control(pDX, IDC_EDITIMU, m_editImu);
	DDX_Control(pDX, IDC_EDITLOG, m_editLog);
	DDX_Text(pDX, IDC_FILENAME, Controller.m_basePathAndName);
	DDX_Control(pDX, IDC_PLAYERFILENAME, m_editPlayerFilePathAndName);
	DDX_Control(pDX, IDC_SLIDER, m_slider);
	DDX_Control(pDX, IDC_CHECKDOBODYTRACKING, m_doBodyTracking);
	DDX_Control(pDX, IDC_CHECKSKELETONS, m_checkSkeletons);
	DDX_Control(pDX, IDC_CHECKCOLOR, m_checkColor);
	DDX_Control(pDX, IDC_CHECKCOLORALIGNED, m_checkColorAligned);
	DDX_Control(pDX, IDC_CHECKDEPTH, m_checkDepth);
	DDX_Control(pDX, IDC_CHECKDEPTHALIGNED, m_checkDepthAligned);
	DDX_Control(pDX, IDC_CHECKIR, m_checkIr);
	DDX_Control(pDX, IDC_CHECKIRALIGNED, m_checkIrAligned);
	DDX_Control(pDX, IDC_CHECKPOINTCLOUDCOLOR, m_checkPointcloudsColor);
	DDX_Control(pDX, IDC_CHECKPOINTCLOUDDEPTH, m_checkPointcloudsDepth);
	DDX_Control(pDX, IDC_PROGRESSEXPORT, m_progressExport);
	DDX_Control(pDX, IDC_FRAMEINDEX, m_editFrameIndex);
	DDX_Text(pDX, IDC_SNAPNAME, m_SnapshotPath);
}

BEGIN_MESSAGE_MAP(CAzureKinectDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDCANCEL, &CAzureKinectDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_STARTCAPTURE, &CAzureKinectDlg::OnBnClickedStartcapture)
	ON_BN_CLICKED(IDC_STOPCAPTURE, &CAzureKinectDlg::OnBnClickedStopcapture)
	ON_CBN_SELCHANGE(IDC_COMBOCONFIG, &CAzureKinectDlg::OnCbnSelchangeComboconfig)
	ON_BN_CLICKED(IDC_AUTOEXP, &CAzureKinectDlg::OnBnClickedAutoexp)
	ON_BN_CLICKED(IDC_MANUALEXP, &CAzureKinectDlg::OnBnClickedManualexp)
	ON_EN_CHANGE(IDC_EDITEXP, &CAzureKinectDlg::OnEnChangeEditexp)
	ON_BN_CLICKED(IDC_STARTRECORD, &CAzureKinectDlg::OnBnClickedStartrecord)
	ON_BN_CLICKED(IDC_STOPRECORD, &CAzureKinectDlg::OnBnClickedStoprecord)
	ON_BN_CLICKED(IDC_SETREFERENCEFILE, &CAzureKinectDlg::OnBnClickedSetreferencefile)
	ON_BN_CLICKED(IDC_CLOSEPLAYER, &CAzureKinectDlg::OnBnClickedCloseplayer)
	ON_BN_CLICKED(IDC_STARTEXPORT, &CAzureKinectDlg::OnBnClickedStartexport)
	ON_BN_CLICKED(IDC_CHECKSKELETONS, &CAzureKinectDlg::OnBnClickedCheckskeletons)
	ON_BN_CLICKED(IDC_STOPEXPORT, &CAzureKinectDlg::OnBnClickedStopexport)
	ON_BN_CLICKED(IDC_CHECKPOINTCLOUDCOLOR, &CAzureKinectDlg::OnBnClickedCheckpointcloudcolor)
	ON_BN_CLICKED(IDC_CHECKPOINTCLOUDDEPTH, &CAzureKinectDlg::OnBnClickedCheckpointclouddepth)
	ON_BN_CLICKED(IDC_LOCALMASTER, &CAzureKinectDlg::OnBnClickedLocalmaster)
	ON_BN_CLICKED(IDC_REMOTEMASTER, &CAzureKinectDlg::OnBnClickedRemotemaster)
	ON_BN_CLICKED(IDC_STANDALONE, &CAzureKinectDlg::OnBnClickedStandalone)
	ON_BN_CLICKED(IDC_CHECKCOLOR, &CAzureKinectDlg::OnBnClickedCheckcolor)
	ON_BN_CLICKED(IDC_CHECKDEPTHALIGNED, &CAzureKinectDlg::OnBnClickedCheckdepthaligned)
	ON_BN_CLICKED(IDC_CHECKIRALIGNED, &CAzureKinectDlg::OnBnClickedCheckiraligned)
	ON_BN_CLICKED(IDC_CHECKCOLORALIGNED, &CAzureKinectDlg::OnBnClickedCheckcoloraligned)
	ON_BN_CLICKED(IDC_CHECKDEPTH, &CAzureKinectDlg::OnBnClickedCheckdepth)
	ON_BN_CLICKED(IDC_CHECKIR, &CAzureKinectDlg::OnBnClickedCheckir)
	ON_BN_CLICKED(IDC_CHECKDOBODYTRACKING, &CAzureKinectDlg::OnBnClickedCheckdobodytracking)
	ON_BN_CLICKED(IDC_CHANGE, &CAzureKinectDlg::OnBnClickedChange)
	ON_BN_CLICKED(IDC_TAKE, &CAzureKinectDlg::OnBnClickedTake)
END_MESSAGE_MAP()


// CAzureKinectDlg message handlers

BOOL CAzureKinectDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	m_comboConfig.AddString(L"Color: 2048x1536 (4:3), Depth: 640x576 (Narrow), @ 30 fps");
	m_comboConfig.AddString(L"Color: 4096x3072 (4:3), Depth: 640x576 (Narrow), @ 15 fps");
	m_comboConfig.AddString(L"Color: 2048x1536 (4:3), Depth: 512x512 (Wide), @ 30 fps");
	m_comboConfig.AddString(L"Color: 4096x3072 (4:3), Depth: 1024x1024 (Wide), @ 15 fps");
	
	m_comboConfig.AddString(L"Color: QHD (16:9), Depth: 640x576 (Narrow), @ 30 fps");
	m_comboConfig.AddString(L"Color: 4K (16:9), Depth: 640x576 (Narrow), @ 15 fps");
	m_comboConfig.AddString(L"Color: QHD (16:9), Depth: 512x512 (Wide), @ 30 fps");
	m_comboConfig.AddString(L"Color: 4K (16:9), Depth: 1024x1024 (Wide), @ 15 fps");

	m_comboConfig.AddString(L"Color: FHD(16:9), Depth : 640x576(Narrow), @ 30 fps");
	m_comboConfig.AddString(L"Color: 720p(16:9), Depth : 640x576(Narrow), @ 30 fps");
		
	m_comboConfig.SetCurSel(0);
	Controller.Kinects.m_inputConfig = (AK::EInputConfig) 0;

	m_radioAutoExp.SetCheck(BST_CHECKED);
	m_radioManualExp.SetCheck(BST_UNCHECKED);
	
	Controller.Kinects.i_extSync = 1;
	m_radioStandalone.SetCheck(BST_UNCHECKED);
	m_radioLocalmaster.SetCheck(BST_CHECKED);
	m_radioRemotemaster.SetCheck(BST_UNCHECKED);

	Controller.b_doBodyTracking = true;
	Player.b_doBodyTracking = true;
	m_doBodyTracking.SetCheck(BST_CHECKED);

	Player.b_convertSkeletons = true;
	m_checkSkeletons.SetCheck(BST_CHECKED);
	Player.b_convertPngColor = false;
	m_checkColor.SetCheck(BST_UNCHECKED);
	Player.b_convertPngDepthAligned = false;
	m_checkDepthAligned.SetCheck(BST_UNCHECKED);
	Player.b_convertPngIrAligned = false;
	m_checkIrAligned.SetCheck(BST_UNCHECKED);
	Player.b_convertPngColorAligned = false;
	m_checkColorAligned.SetCheck(BST_UNCHECKED);
	Player.b_convertPngDepth = false;
	m_checkDepth.SetCheck(BST_UNCHECKED);
	Player.b_convertPngIr = false;
	m_checkIr.SetCheck(BST_UNCHECKED);
	Player.b_convertPointcloudColor = false;
	m_checkPointcloudsColor.SetCheck(BST_UNCHECKED);
	Player.b_convertPointcloudDepthColor = false;
	m_checkPointcloudsDepth.SetCheck(BST_UNCHECKED);

	m_editExp.SetWindowText(L"0");
	Controller.exposure = 0;
	m_progressExport.SetRange(0, 1);
	m_progressExport.SetPos(0);

	// Increase the text limit to the maximum
	m_editLog.SetLimitText(0);

	Controller.Display.SetContainer(this);
	Controller.Kinects.SetContainer(this);
	Player.Display.SetContainer(this);
	Player.VirtualKinects.SetContainer(this);

	/*TCHAR my_documents[MAX_PATH];
	HRESULT result = SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, my_documents);
	m_SnapshotPath = my_documents;*/

	TCHAR* my_downloads;
	HRESULT result = SHGetKnownFolderPath(FOLDERID_Downloads, KF_FLAG_DEFAULT, NULL, &my_downloads);
	m_SnapshotPath = my_downloads;
	Controller.Display.captureDestinationFolder = CT2A(m_SnapshotPath);
	
	UpdateData(FALSE);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CAzureKinectDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAzureKinectDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAzureKinectDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CAzureKinectDlg::OnBnClickedCancel()
{
	if (Controller.b_acqLoop) 
		OnBnClickedStopcapture();
	
	if (Controller.m_hThread  && Controller.b_existThread == true)
		cv::waitKey(2500);
	else if (Controller.m_hThread  && Controller.b_existThread == false)
		cv::waitKey(1500);

	CDialogEx::OnCancel();
}


void CAzureKinectDlg::OnBnClickedStartcapture()
{	
	if (Controller.m_hThread  && Controller.b_existThread == true)
		cv::waitKey(2500);
	else if (Controller.m_hThread  && Controller.b_existThread == false)
		cv::waitKey(1500);
	
	Controller.b_isInitThreadEnded = false;
	Controller.CreateThread();
	while (!Controller.b_isInitThreadEnded) {
		cv::waitKey(10);
	}
	if (Controller.m_hThread) {
		Controller.b_acqLoop = true;
		GetDlgItem(IDC_STARTCAPTURE)->EnableWindow(FALSE);
		GetDlgItem(IDC_STOPCAPTURE)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBOCONFIG)->EnableWindow(FALSE);
		GetDlgItem(IDC_STANDALONE)->EnableWindow(FALSE);
		GetDlgItem(IDC_LOCALMASTER)->EnableWindow(FALSE);
		GetDlgItem(IDC_REMOTEMASTER)->EnableWindow(FALSE);
		GetDlgItem(IDC_SETREFERENCEFILE)->EnableWindow(FALSE);
		GetDlgItem(IDC_STARTRECORD)->EnableWindow(TRUE);
		GetDlgItem(IDC_TAKE)->EnableWindow(TRUE);
		if (Controller.nKinects > 1) {
			Controller.b_doBodyTracking = false;
			Player.b_doBodyTracking = false;
			m_doBodyTracking.SetCheck(BST_UNCHECKED);
			GetDlgItem(IDC_CHECKDOBODYTRACKING)->EnableWindow(FALSE);
		}
	}
	else {
		AfxMessageBox(L"No Kinect Connected");
		OnBnClickedStopcapture();
	}		
}


void CAzureKinectDlg::OnBnClickedStopcapture()
{
	GetDlgItem(IDC_STOPCAPTURE)->EnableWindow(FALSE);
	Controller.b_takeSnapshot = false;
	Controller.b_acqLoop = false;
	Controller.m_basePathAndName.Format(L"");
	m_editImu.SetWindowTextW(L"");
	GetDlgItem(IDC_STARTCAPTURE)->EnableWindow(TRUE);
	GetDlgItem(IDC_COMBOCONFIG)->EnableWindow(TRUE);
	GetDlgItem(IDC_STANDALONE)->EnableWindow(TRUE);
	GetDlgItem(IDC_LOCALMASTER)->EnableWindow(TRUE);
	GetDlgItem(IDC_REMOTEMASTER)->EnableWindow(TRUE);
	GetDlgItem(IDC_STARTRECORD)->EnableWindow(FALSE);
	GetDlgItem(IDC_STOPRECORD)->EnableWindow(FALSE);
	GetDlgItem(IDC_SETREFERENCEFILE)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECKDOBODYTRACKING)->EnableWindow(TRUE);
	GetDlgItem(IDC_TAKE)->EnableWindow(FALSE);
	UpdateData(FALSE);
}


void CAzureKinectDlg::OnCbnSelchangeComboconfig()
{
	int configSel = m_comboConfig.GetCurSel();
	if (configSel != LB_ERR)
		Controller.Kinects.m_inputConfig = (AK::EInputConfig)m_comboConfig.GetCurSel();
	else
		Controller.Kinects.m_inputConfig = AK::COLOR43_2048x1536_DEPTHN_640x576_30FPS;
}


void CAzureKinectDlg::OnBnClickedStandalone()
{
	m_radioStandalone.SetCheck(BST_CHECKED);
	m_radioLocalmaster.SetCheck(BST_UNCHECKED);
	m_radioRemotemaster.SetCheck(BST_UNCHECKED);
	Controller.Kinects.i_extSync = 0;
}


void CAzureKinectDlg::OnBnClickedLocalmaster()
{
	m_radioStandalone.SetCheck(BST_UNCHECKED);
	m_radioLocalmaster.SetCheck(BST_CHECKED);
	m_radioRemotemaster.SetCheck(BST_UNCHECKED);
	Controller.Kinects.i_extSync = 1;
}


void CAzureKinectDlg::OnBnClickedRemotemaster()
{
	m_radioStandalone.SetCheck(BST_UNCHECKED);
	m_radioLocalmaster.SetCheck(BST_UNCHECKED);
	m_radioRemotemaster.SetCheck(BST_CHECKED);
	Controller.Kinects.i_extSync = 2;
}


void CAzureKinectDlg::OnBnClickedAutoexp()
{
	GetDlgItem(IDC_EDITEXP)->EnableWindow(FALSE);
	m_radioAutoExp.SetCheck(BST_CHECKED);
	m_radioManualExp.SetCheck(BST_UNCHECKED);
	m_editExp.SetWindowText(L"0");

	if (Controller.b_acqLoop) {
		Controller.exposure = 0;
		Controller.b_setExposuresNow = true;
	}		
}


void CAzureKinectDlg::OnBnClickedManualexp()
{
	GetDlgItem(IDC_EDITEXP)->EnableWindow(TRUE);
	m_radioAutoExp.SetCheck(BST_UNCHECKED);
	m_radioManualExp.SetCheck(BST_CHECKED);
}


void CAzureKinectDlg::OnEnChangeEditexp()
{
	CString tString = L"";
	m_editExp.GetWindowTextW(tString);

	if (tString.SpanIncluding(L"0123456789") != tString)
		m_editExp.SetWindowText(L"0");
	else {
		if (_wtoi(tString.GetBuffer()) > (int)(1000000.0 / (double)Controller.Kinects.m_fps)) {
			CString ttString;
			ttString.Format(L"%d", (int)(1000000.0 / (double)Controller.Kinects.m_fps));
			m_editExp.SetWindowText(ttString);
		}
			
		else if (_wtoi(tString.GetBuffer()) < 0)
			m_editExp.SetWindowText(L"0");
	}
	Controller.exposure = _wtoi(tString.GetBuffer());
	Controller.b_setExposuresNow = true;
}


void CAzureKinectDlg::OnBnClickedStartrecord()
{
	GetDlgItem(IDC_STARTRECORD)->EnableWindow(FALSE);
	GetDlgItem(IDC_STOPRECORD)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECKDOBODYTRACKING)->EnableWindow(FALSE);
	GetDlgItem(IDC_TAKE)->EnableWindow(FALSE);

	COutputDlg OutputDlg;
	std::string strName = GetCurrentDateTime();
	char tName[80];
	strcpy_s(tName, 80, strName.c_str());
	OutputDlg.m_FileName = tName;

	TCHAR* my_downloads;
	HRESULT result = SHGetKnownFolderPath(FOLDERID_Downloads, KF_FLAG_DEFAULT, NULL, &my_downloads);
	OutputDlg.m_FilePath = my_downloads;
	
	/*TCHAR my_documents[MAX_PATH];
	HRESULT result = SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, my_documents);
	OutputDlg.m_FilePath = my_documents;*/

	if (OutputDlg.DoModal() == IDOK) {
		int extPos = OutputDlg.m_FileName.Find(L".", OutputDlg.m_FileName.GetLength() - 4);
		if (extPos >= 0)
			Controller.m_basePathAndName = OutputDlg.m_FilePath + "\\" + OutputDlg.m_FileName.Left(extPos);
		else
			Controller.m_basePathAndName = OutputDlg.m_FilePath + "\\" + OutputDlg.m_FileName;
		Controller.b_startRecording = true;
		UpdateData(FALSE);
	}
	else {
		AfxMessageBox(L"Cannot Open File");
		GetDlgItem(IDC_STARTRECORD)->EnableWindow(TRUE);
		GetDlgItem(IDC_STOPRECORD)->EnableWindow(FALSE);
		GetDlgItem(IDC_TAKE)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECKDOBODYTRACKING)->EnableWindow(TRUE);
	}

	
	cv::waitKey(1000 * static_cast<int>(Controller.nKinects));
	/*if (!Controller.b_isRecording) {
		GetDlgItem(IDC_STARTRECORD)->EnableWindow(TRUE);
		GetDlgItem(IDC_STOPRECORD)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECKDOBODYTRACKING)->EnableWindow(TRUE);
	}
	*/
}


void CAzureKinectDlg::OnBnClickedStoprecord()
{
	Controller.b_stopRecording = true;
	Controller.m_basePathAndName.Format(L"");
	GetDlgItem(IDC_STARTRECORD)->EnableWindow(TRUE);
	GetDlgItem(IDC_STOPRECORD)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKDOBODYTRACKING)->EnableWindow(TRUE);
	GetDlgItem(IDC_TAKE)->EnableWindow(TRUE);
	UpdateData(FALSE);
}


const std::string CAzureKinectDlg::GetCurrentDateTime()
{
	time_t     now = time(0);
	struct tm  tstruct;
	localtime_s(&tstruct, &now);
	char       buf[80];
	strftime(buf, sizeof(buf), "%Y%m%d_%H%M%S", &tstruct);
	return buf;
}

void CAzureKinectDlg::OnBnClickedSetreferencefile()
{
	Player.m_BaseFilePathAndName.Format(L"");
	m_editPlayerFilePathAndName.SetWindowTextW(Player.m_BaseFilePathAndName);

	CFileDialog FileDlg(TRUE, L"", L"*.mkv");
	FileDlg.m_ofn.lpstrTitle = L"Set the reference ''.mkv'' file";
	if (FileDlg.DoModal() == IDOK) {
		Player.m_BaseFilePathAndName = FileDlg.GetPathName();
		m_editPlayerFilePathAndName.SetWindowTextW(Player.m_BaseFilePathAndName);

		// Separate the folder path
		CString folderPath = Player.m_BaseFilePathAndName.Left(Player.m_BaseFilePathAndName.GetLength() - FileDlg.GetFileName().GetLength());
		// Convert a TCHAR string to a LPCSTR and assign to InputPath
		CT2CA pszConvertedFolder(folderPath);
		Player.InputPath = pszConvertedFolder;

		// Assigne the name of the refeference camera bag
		// Convert a TCHAR string to a LPCSTR and assign to RefCamFileName
		CT2CA pszConvertedFile(FileDlg.GetFileName());
		Player.RefCamFileName = pszConvertedFile;
		
		if (Player.InitFileNames() < 0) {
			AfxMessageBox(L"Cannot Open File");
			return;
		}
		
		Player.InitPlaybacks();
		if (Player.nFramesMin <= 0) {
			AfxMessageBox(L"Cannot Init Playbacks");
			return;
		}

		if (Player.VirtualKinects.Count() > 1) {
			Controller.b_doBodyTracking = false;
			Player.b_doBodyTracking = false;
			m_doBodyTracking.SetCheck(BST_UNCHECKED);
			GetDlgItem(IDC_CHECKDOBODYTRACKING)->EnableWindow(FALSE);
		}

		Player.b_isFirstFrame = true;
		Player.DisplayFramesAt(0);

		GetDlgItem(IDC_CLOSEPLAYER)->EnableWindow(TRUE);
		GetDlgItem(IDC_STARTEXPORT)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECKSKELETONS)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECKCOLOR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECKCOLORALIGNED)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECKDEPTH)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECKDEPTHALIGNED)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECKIR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECKIRALIGNED)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECKPOINTCLOUDCOLOR)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECKPOINTCLOUDDEPTH)->EnableWindow(TRUE);
		GetDlgItem(IDC_SLIDER)->EnableWindow(TRUE);
		GetDlgItem(IDC_SETREFERENCEFILE)->EnableWindow(FALSE);
		GetDlgItem(IDC_STARTCAPTURE)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBOCONFIG)->EnableWindow(FALSE);
		GetDlgItem(IDC_STANDALONE)->EnableWindow(FALSE);
		GetDlgItem(IDC_LOCALMASTER)->EnableWindow(FALSE);
		GetDlgItem(IDC_REMOTEMASTER)->EnableWindow(FALSE);
		GetDlgItem(IDC_AUTOEXP)->EnableWindow(FALSE);
		GetDlgItem(IDC_MANUALEXP)->EnableWindow(FALSE);


		m_editFrameIndex.SetWindowTextW(L"0");
		m_slider.SetRangeMin(0, false);
		m_slider.SetRangeMax(Player.nFramesMin, false);

	}
	else
		AfxMessageBox(L"Cannot Open File");
}


void CAzureKinectDlg::OnBnClickedCloseplayer()
{
	Player.ClosePlaybacks(Player.b_doBodyTracking);
	m_editPlayerFilePathAndName.SetWindowTextW(Player.m_BaseFilePathAndName);
	GetDlgItem(IDC_CLOSEPLAYER)->EnableWindow(FALSE);
	GetDlgItem(IDC_STARTEXPORT)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKSKELETONS)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKCOLOR)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKCOLORALIGNED)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKDEPTH)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKDEPTHALIGNED)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKIR)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKIRALIGNED)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKPOINTCLOUDCOLOR)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKPOINTCLOUDDEPTH)->EnableWindow(FALSE);
	GetDlgItem(IDC_SLIDER)->EnableWindow(FALSE);
	GetDlgItem(IDC_SETREFERENCEFILE)->EnableWindow(TRUE);
	GetDlgItem(IDC_STARTCAPTURE)->EnableWindow(TRUE);
	GetDlgItem(IDC_COMBOCONFIG)->EnableWindow(TRUE);
	GetDlgItem(IDC_STANDALONE)->EnableWindow(TRUE);
	GetDlgItem(IDC_LOCALMASTER)->EnableWindow(TRUE);
	GetDlgItem(IDC_REMOTEMASTER)->EnableWindow(TRUE);
	GetDlgItem(IDC_AUTOEXP)->EnableWindow(TRUE);
	GetDlgItem(IDC_MANUALEXP)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECKDOBODYTRACKING)->EnableWindow(TRUE);

	m_editImu.SetWindowTextW(L"");
	m_slider.SetPos(0);
	m_editFrameIndex.SetWindowTextW(L"");
}

void CAzureKinectDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	/*int newPos;
	if (IDC_SLIDER == pScrollBar->GetDlgCtrlID() && !Player.b_isProcessing)
	{
		// When there is no member variable and we need to access the control
		CSliderCtrl* pSlider = reinterpret_cast<CSliderCtrl*>(pScrollBar);
		// Handle event here
		switch (nSBCode) {
		case TB_LINEUP:
		case TB_LINEDOWN:
		case TB_PAGEUP:
		case TB_PAGEDOWN:
		case TB_THUMBPOSITION:
			SCROLLINFO si;
			si.cbSize = sizeof(si);
			si.fMask = SIF_ALL;
			::GetScrollInfo(pScrollBar->m_hWnd, SB_HORZ, &si);
			//pScrollBar->GetScrollInfo(&si);
			// Set nPos to the 32-bit value
			nPos = si.nTrackPos;
			Player.DisplayFramesAt(nPos);
			break;
		case TB_TOP:
		case TB_BOTTOM:
		case TB_THUMBTRACK:
		case TB_ENDTRACK:
		default:
			break;
		}
		//return;
		m_slider.SetPos(Player.m_framePos);
		CString tString;
		tString.Format(L"%d", Player.m_framePos);
		m_editFrameIndex.SetWindowTextW(tString);
	}*/
	
	int newPos;
	
	if (pScrollBar == (CScrollBar *)&m_slider && !Player.b_isProcessing) {
		switch (nSBCode) {
		case SB_THUMBTRACK:
			newPos = m_slider.GetPos();
			Player.DisplayFramesAt(newPos);
			break;
		case SB_LINELEFT:
			Player.DisplayFramesAt(MAX(Player.m_framePos - 1, 0));
			break;
		case SB_LINERIGHT:
			Player.DisplayFramesAt(MIN(Player.m_framePos + 1, Player.nFramesMin));
			break;
		case SB_PAGELEFT:
			newPos = m_slider.GetPos();
			Player.DisplayFramesAt(newPos);
			break;
		case SB_PAGERIGHT:
			newPos = m_slider.GetPos();
			Player.DisplayFramesAt(newPos);
			break;
		default:
			break;
		}
		m_slider.SetPos(Player.m_framePos);
		CString tString;
		tString.Format(L"%d", Player.m_framePos);
		m_editFrameIndex.SetWindowTextW(tString);
	}
	

	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}


void CAzureKinectDlg::OnBnClickedStartexport()
{
	GetDlgItem(IDC_CLOSEPLAYER)->EnableWindow(FALSE);
	GetDlgItem(IDC_STARTEXPORT)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKSKELETONS)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKCOLOR)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKCOLORALIGNED)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKDEPTH)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKDEPTHALIGNED)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKIR)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKIRALIGNED)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKPOINTCLOUDCOLOR)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECKPOINTCLOUDDEPTH)->EnableWindow(FALSE);
	GetDlgItem(IDC_SLIDER)->EnableWindow(FALSE);
	GetDlgItem(IDC_STOPEXPORT)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECKDOBODYTRACKING)->EnableWindow(FALSE);
	m_editImu.SetWindowTextW(L"");
	m_progressExport.EnableWindow(TRUE);

	size_t nConverters = Player.InitConverters(GetCurrentDateTime());
	m_progressExport.SetRange(1, 1000);
	if (nConverters > 0) {
		for (int pos = 0; pos < Player.nFramesMin; pos++) {
			cv::waitKey(1);
			if (Player.b_stopConversion) {
				Player.b_stopConversion = false;
				break;
			}
			int newPos = (pos + 1) * 1000 / Player.nFramesMin;
			m_progressExport.SetPos(newPos);
			Player.ConvertFramesAt(pos);			
		}
		Player.Converters.clear();
	}
	else
		AfxMessageBox(L"No output selected");

	GetDlgItem(IDC_CLOSEPLAYER)->EnableWindow(TRUE);
	GetDlgItem(IDC_STARTEXPORT)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECKSKELETONS)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECKCOLOR)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECKCOLORALIGNED)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECKDEPTH)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECKDEPTHALIGNED)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECKIR)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECKIRALIGNED)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECKPOINTCLOUDCOLOR)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECKPOINTCLOUDDEPTH)->EnableWindow(TRUE);
	GetDlgItem(IDC_SLIDER)->EnableWindow(TRUE);
	GetDlgItem(IDC_STOPEXPORT)->EnableWindow(FALSE);
	if (Player.VirtualKinects.Count() == 1)
		GetDlgItem(IDC_CHECKDOBODYTRACKING)->EnableWindow(TRUE);
	m_progressExport.EnableWindow(FALSE);
	m_progressExport.SetPos(0);
}


void CAzureKinectDlg::OnBnClickedCheckskeletons()
{
	Player.b_convertSkeletons = !Player.b_convertSkeletons;
}


void CAzureKinectDlg::OnBnClickedStopexport()
{
	Player.b_stopConversion = true;
	if (Player.VirtualKinects.Count() == 1) 
		GetDlgItem(IDC_CHECKDOBODYTRACKING)->EnableWindow(TRUE);
}


void CAzureKinectDlg::OnBnClickedCheckpointcloudcolor()
{
	Player.b_convertPointcloudColor = !Player.b_convertPointcloudColor;
}

void CAzureKinectDlg::OnBnClickedCheckpointclouddepth()
{
	Player.b_convertPointcloudDepthColor = !Player.b_convertPointcloudDepthColor;
}


void CAzureKinectDlg::OnBnClickedCheckcolor()
{
	Player.b_convertPngColor = !Player.b_convertPngColor;
}


void CAzureKinectDlg::OnBnClickedCheckdepthaligned()
{
	Player.b_convertPngDepthAligned = !Player.b_convertPngDepthAligned;
}


void CAzureKinectDlg::OnBnClickedCheckiraligned()
{
	Player.b_convertPngIrAligned = !Player.b_convertPngIrAligned;
}


void CAzureKinectDlg::OnBnClickedCheckcoloraligned()
{
	Player.b_convertPngColorAligned = !Player.b_convertPngColorAligned;
}


void CAzureKinectDlg::OnBnClickedCheckdepth()
{
	Player.b_convertPngDepth = !Player.b_convertPngDepth;
}


void CAzureKinectDlg::OnBnClickedCheckir()
{
	Player.b_convertPngIr = !Player.b_convertPngIr;
}


void CAzureKinectDlg::OnBnClickedCheckdobodytracking()
{
	Controller.b_doBodyTracking = !Controller.b_doBodyTracking;
	Controller.b_isDoBodyTrackingChanged = true;
	Player.b_doBodyTracking = !Player.b_doBodyTracking;
	Player.b_isDoBodyTrackingChanged = true;

	CString tString;
	if (Controller.b_doBodyTracking)
		tString.Format(L"Body tracking enabled\r\n");
	else
		tString.Format(L"Body tracking disabled\r\n");
	Controller.Kinects.AppendLogMessages(tString);
}


void CAzureKinectDlg::OnBnClickedChange()
{
	// Example code
	CFolderPickerDialog FolderDlg;

	FolderDlg.m_ofn.lpstrTitle = _T("Select the path of the destination folder");
	FolderDlg.m_ofn.lpstrInitialDir = m_SnapshotPath;
	if (FolderDlg.DoModal() == IDOK) {
		m_SnapshotPath = FolderDlg.GetPathName();
		Controller.Display.captureDestinationFolder = CT2A(m_SnapshotPath);
		UpdateData(FALSE);
	}
	else {
		AfxMessageBox(L"Destination path not selected");
	}
}


void CAzureKinectDlg::OnBnClickedTake()
{
	Controller.b_takeSnapshot = true;
}
