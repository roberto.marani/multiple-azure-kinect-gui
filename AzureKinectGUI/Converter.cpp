#include "stdafx.h"
#include "Converter.h"


CConverter::CConverter()
{
}


CConverter::~CConverter()
{
}


void CConverter::SetConverter(conversion_type cType, std::string inDestination, int inRefKinect)
{
	destination = inDestination;
	conversionType = cType;
	referenceKinect = inRefKinect;
	std::filesystem::create_directories(inDestination);
}


void CConverter::SetSkeletons(std::vector<AK::SSkeleton> inSkeletons)
{
	skeletons = inSkeletons;
}


void CConverter::SetPointcloud(AK::SPointcloud inCloud)
{
	cloud = inCloud;
}

void CConverter::SetImage(AK::SImage inImage)
{
	image = inImage;
}

void CConverter::SetSupportImage(AK::SImage inSupportImage)
{
	supportImage = inSupportImage;
}


void CConverter::StartConversionThreads()
{
	switch (conversionType)
	{
	case CONVERT_PNG_COLOR: case CONVERT_PNG_COLOR_ALIGNED: case CONVERT_PNG_DEPTH: case CONVERT_PNG_DEPTH_ALIGNED: case CONVERT_PNG_IR: case CONVERT_PNG_IR_ALIGNED:
		ConvertPng();
		break;
	case CONVERT_RAW_COLOR: case CONVERT_RAW_COLOR_ALIGNED: case CONVERT_RAW_DEPTH: case CONVERT_RAW_DEPTH_ALIGNED: case CONVERT_RAW_IR: case CONVERT_RAW_IR_ALIGNED:
		ConvertRaw();
		break;
	case CONVERT_SKELETONS:
		ConvertSkeletons();
		break;
	case CONVERT_POINTCLOUDSCOLOR:
		ConvertPointclouds();
		break;
	case CONVERT_POINTCLOUDSDEPTH:
		ConvertPointclouds();
		break;
	}
}

void CConverter::EndConversionThreads()
{
	worker.join();
}

void CConverter::ConvertSkeletons()
{
	start_worker([this] {

		if (skeletons.size() > 0) {
			std::ofstream ofs;

			// Set the name of the skeleton txt file(destination is the path)
			char filenameC[256];
			sprintf_s(filenameC, "%s/FrameID%06d_DeviceTimeStamp%" PRId64 "us.txt", destination.c_str(), frameID, skeletons[0].Timestamp.Device.count());

			// Open file for writing
			ofs.open(filenameC, std::ofstream::out | std::ofstream::app);
			char firstLine[256];
			sprintf_s(firstLine, 256, "BodyID\tJointID\tConfidance\tX\tY\tZ\tQw\tQx\tQy\tQz\tX2DColor\tY2DColor\tX2DDepth\tY2DDepth");
			ofs << firstLine << std::endl;

			if (ofs.is_open()) {
				// Write the skeletons to the output file
				char SkelStr[256];
				for (int i = 0; i < skeletons.size(); i++) {
					for (int j = 0; j < 32; j++) {
						sprintf_s(SkelStr, 256, "%u\t%d\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f",
							skeletons[i].BodyID, j, skeletons[i].Confidence[j],
							skeletons[i].Joint3D[j].Position[0], skeletons[i].Joint3D[j].Position[1], skeletons[i].Joint3D[j].Position[2],
							skeletons[i].Joint3D[j].Orientation[0], skeletons[i].Joint3D[j].Orientation[1], skeletons[i].Joint3D[j].Orientation[2], skeletons[i].Joint3D[j].Orientation[3],
							skeletons[i].Joint2DColorSpace[j].x, skeletons[i].Joint2DColorSpace[j].y,
							skeletons[i].Joint2DDepthSpace[j].x, skeletons[i].Joint2DDepthSpace[j].y);
						ofs << SkelStr << std::endl;
					}
				}
				ofs.close();
			}
			skeletons.clear();
		}		
	});
}

void CConverter::ConvertPointclouds()
{
	start_worker([this] {
		if (cloud.Points.size() > 0) {
			
			std::ofstream ofs;

			// Set the name of the ply file (destination is the path)
			char filenameC[256];
			sprintf_s(filenameC, "%s/FrameID%06d_DeviceTimeStamp%" PRId64 "us.ply", destination.c_str(), frameID, cloud.Timestamp.Device.count());

			// Open file for writing
			ofs.open(filenameC, std::ofstream::out | std::ofstream::app);

			if (ofs.is_open()) {
				ofs << "ply" << std::endl;
				//ofs << "format ascii 1.0" << std::endl;
				ofs << "format binary_little_endian 1.0" << std::endl;
				ofs << "element vertex" << " " << cloud.Points.size() << std::endl;
				ofs << "property float x" << std::endl;
				ofs << "property float y" << std::endl;
				ofs << "property float z" << std::endl;
				ofs << "property uchar blue" << std::endl;
				ofs << "property uchar green" << std::endl;
				ofs << "property uchar red" << std::endl;
				ofs << "end_header" << std::endl;
			}
			ofs.close();

			std::ofstream ofsbin;
			ofsbin.open(filenameC, std::ofstream::out | std::ofstream::app | std::ofstream::binary);
			if (ofsbin.is_open()) {

				for (int i = 0; i < cloud.Points.size(); i++) {
					ofsbin.write(reinterpret_cast<char*>(&cloud.Points[i].x), sizeof(float));
					ofsbin.write(reinterpret_cast<char*>(&cloud.Points[i].y), sizeof(float));
					ofsbin.write(reinterpret_cast<char*>(&cloud.Points[i].z), sizeof(float));
					ofsbin.write(reinterpret_cast<char*>(&cloud.Points[i].b), sizeof(uint8_t));
					ofsbin.write(reinterpret_cast<char*>(&cloud.Points[i].g), sizeof(uint8_t));
					ofsbin.write(reinterpret_cast<char*>(&cloud.Points[i].r), sizeof(uint8_t));
				}
				ofsbin.close();

				/*char PointStr[256];
				std::stringstream ss;
				for (int i = 0; i < cloud.Points.size(); ++i) {
					sprintf_s(PointStr, 256, "%.6f %.6f %.6f %d %d %d", cloud.Points[i].x, cloud.Points[i].y, cloud.Points[i].z, cloud.Points[i].b, cloud.Points[i].g, cloud.Points[i].r);
					ss << PointStr << std::endl;
				}
				ofs.write(ss.str().c_str(), (std::streamsize)ss.str().length());
				ofs.close();*/
			}
			cloud.Points.clear();
		}		
	});
}

void CConverter::ConvertPng()
{
	start_worker([this] {

		if (image.Width > 0) {
			cv::Mat outputImage;

			if (conversionType == CONVERT_PNG_COLOR)  {
				cv::Mat imageRGBraw(1, 4 * image.Height * image.Width, CV_8UC1, (void*)image.Data.data());
				outputImage = imdecode(imageRGBraw, cv::IMREAD_COLOR);
				//cv::cvtColor(outputImage, outputImage, cv::COLOR_BGR2BGRA);
			}
			else if (conversionType == CONVERT_PNG_COLOR_ALIGNED) {
				cv::Mat dImage(supportImage.Height, supportImage.Width, CV_16UC1, supportImage.Data.data());
				cv::Mat mask;
				cv::inRange(dImage, cv::Scalar(0), cv::Scalar(0), mask);
				outputImage = cv::Mat(image.Height, image.Width, CV_8UC4, (void*)image.Data.data(), cv::Mat::AUTO_STEP);
				outputImage.setTo(cv::Scalar(0, 0, 0, 0), mask);
			}
			else if (conversionType == CONVERT_PNG_DEPTH || conversionType == CONVERT_PNG_DEPTH_ALIGNED) {
				outputImage = cv::Mat(image.Height, image.Width, CV_16UC1, image.Data.data());
				/*cv::threshold(outputImage, outputImage, AK::MAX_RANGE, AK::MAX_RANGE, cv::THRESH_TRUNC);
				cv::threshold(outputImage, outputImage, AK::MIN_RANGE, 0, cv::THRESH_TOZERO);
				outputImage = (outputImage - AK::MIN_RANGE) * 65535 / (AK::MAX_RANGE - AK::MIN_RANGE);
				outputImage.convertTo(outputImage, CV_8U, 1.0 / 256.0);
				cv::applyColorMap(outputImage, outputImage, cv::COLORMAP_JET);
				cv::Mat mask;
				cv::inRange(outputImage, cv::Scalar(128, 0, 0), cv::Scalar(128, 0, 0), mask);
				cv::cvtColor(outputImage, outputImage, cv::COLOR_BGR2BGRA);
				outputImage.setTo(cv::Scalar(0, 0, 0, 0), mask);*/	
			}
			else if (conversionType == CONVERT_PNG_IR || conversionType == CONVERT_PNG_IR_ALIGNED) {
				outputImage = cv::Mat(image.Height, image.Width, CV_16UC1, image.Data.data());
				/*cv::Mat mask;
				cv::inRange(outputImage, cv::Scalar(0), cv::Scalar(0), mask);
				cv::merge(std::vector<cv::Mat>{outputImage, outputImage, outputImage}, outputImage);
				cv::cvtColor(outputImage, outputImage, cv::COLOR_BGR2BGRA);
				outputImage.setTo(cv::Scalar(0, 0, 0, 0), mask);*/
			}
	
			// Set the image name (destination is the path)
			char filenameC[256];
			sprintf_s(filenameC, "%s/FrameID%06d_DeviceTimeStamp%" PRId64 "us.png", destination.c_str(), frameID, image.Timestamp.Device.count());
			std::string filenameS(filenameC);

			// Write the image
			cv::imwrite(filenameS, outputImage);

			image.Width = 0;
		}		
	});
}

void CConverter::ConvertRaw()
{
	// To be done (if necessary)
}




int CConverter::GetReferenceKinect()
{
	return referenceKinect;
}

