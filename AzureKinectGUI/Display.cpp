#include "stdafx.h"
#include "Display.h"
#include "AzureKinectGUIDlg.h"

constexpr auto WINW = 1820;
constexpr auto WINH = 600;

CDisplay::CDisplay()
	: bWinOpen(false)
	, iSaveFrame(1)
{
	m_winName1 = "Captures";
	m_winName2 = "Body Tracking";
	//cv::ocl::setUseOpenCL(true);
}


CDisplay::~CDisplay()
{
	cv::destroyAllWindows();
}


void CDisplay::Init(bool withBodyTracking)
{
	// open window
	if (withBodyTracking) {
		cv::namedWindow(m_winName1, cv::WINDOW_NORMAL);
		cv::resizeWindow(m_winName1, WINW, WINH);
		cv::moveWindow(m_winName1, 50, 40);
			
		cv::namedWindow(m_winName2, cv::WINDOW_NORMAL);
		cv::resizeWindow(m_winName2, WINW, (int)(WINH * 1.5));
		cv::moveWindow(m_winName2, 50, 700);
	}
	else {
		cv::namedWindow(m_winName1, cv::WINDOW_NORMAL);
		cv::resizeWindow(m_winName1, WINW, WINH*2);
		cv::moveWindow(m_winName1, 50, 40);
	}

	//Set the icon
	HICON hIcon = (HICON)LoadImageW(NULL, L"res\\Icon.ico", IMAGE_ICON, 32, 32, LR_LOADFROMFILE);
	if (hIcon) {
		HWND winHandle = GetWindowHandle(m_winName1);
		if (winHandle) 
			SendMessage(winHandle, WM_SETICON, ICON_BIG, (LPARAM)hIcon);
		
		winHandle = GetWindowHandle(m_winName2);
		if (winHandle) 
			SendMessage(winHandle, WM_SETICON, ICON_BIG, (LPARAM)hIcon);
		
		DestroyIcon(hIcon);
	}

	bWinOpen = true;
}


void CDisplay::ReInit(bool withBodyTracking)
{
	Close(!withBodyTracking);
	Init(withBodyTracking);
}


HWND CDisplay::GetWindowHandle(std::string inputWinName)
{
	CCountWindows WinCounter;
	int nWindows = WinCounter.CountThem();

	int bufferLength;
	int inLength = (int)inputWinName.length() + 1;
	bufferLength = MultiByteToWideChar(CP_ACP, 0, inputWinName.c_str(), inLength, 0, 0);
	wchar_t* buffer = new wchar_t[bufferLength];
	MultiByteToWideChar(CP_ACP, 0, inputWinName.c_str(), inLength, buffer, bufferLength);
	std::wstring inputWinNameW(buffer);
	delete[] buffer;

	for (int i = 0; i < nWindows; i++) {
		if (WinCounter.WinNames[i].GetBuffer() == inputWinNameW)
			return WinCounter.hWindows[i];
	}
	return NULL;
}


void CDisplay::SetContainer(void * Container)
{
	p_Container = Container;
}


void CDisplay::Close(bool withBodyTracking)
{
	// destroy window
	if (bWinOpen) {
		cv::destroyWindow(m_winName1);
		if (withBodyTracking)
			cv::destroyWindow(m_winName2);
		bWinOpen = false;
	}
}


void CDisplay::ClearVariables()
{
	Colors.clear();
	IRs.clear();
	Depths.clear();

	ColorsA.clear();
	IRsA.clear();
	DepthsA.clear();
	PointcloudsColor.clear();
	PointcloudsDepth.clear();

	BodyIndexMapsA.clear();
	Skeletons.clear();
	BodyIDs.clear();
	IMUs.clear();
}


void CDisplay::ShowCaptures()
{
	std::vector<cv::Mat> ColorMats, IRMats, DepthMats;// , DepthAMats;
	cv::Mat PlotImage;
	std::vector<cv::Mat> PlotImages;

	int cols = 3;
	int rows = (int) nImageMax / cols;
	if (rows == 0)
		rows++;

	int dispW = WINW / cols;
	int dispH = WINH / rows;
	char Testo[256];

	for (int i = 0; i < Colors.size(); i++) {
		cv::Mat tMat;
		if (Colors[i].Width <= 0)
			tMat = cv::Mat::zeros(cv::Size(dispW, dispH), CV_8UC3);
		else {
			cv::Mat rawColorMat(1, 4 * Colors[i].Height * Colors[i].Width, CV_8UC1, (void*)Colors[i].Data.data());
			tMat = imdecode(rawColorMat, cv::IMREAD_COLOR);
			cv::resize(tMat, tMat, cv::Size(dispW, dispH));
		}
		sprintf_s(Testo, "SN: %s", Colors[i].SerialNumber.c_str());
		cv::putText(tMat, Testo, cv::Point(10, 20), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);
		sprintf_s(Testo, "Device Timestamp: %" PRId64 " us", Colors[i].Timestamp.Device.count());
		cv::putText(tMat, Testo, cv::Point(10, 40), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);
		sprintf_s(Testo, "System Timestamp: %" PRId64 " ns", Colors[i].Timestamp.System.count());
		cv::putText(tMat, Testo, cv::Point(10, 60), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);
		ColorMats.push_back(tMat);
	}
	cv::Mat tPlotColor;
	ColorMats[0].copyTo(tPlotColor);
	for (int i = 1; i < ColorMats.size(); i++)
		cv::vconcat(tPlotColor, ColorMats[i], tPlotColor);
	PlotImages.push_back(tPlotColor);

	for (int i = 0; i < IRs.size(); i++) {
		cv::Mat tMat;
		if (IRs[i].Width <= 0)
			tMat = cv::Mat::zeros(cv::Size(dispW, dispH), CV_8UC3);
		else {
			tMat = cv::Mat(IRs[i].Height, IRs[i].Width, CV_16UC1, IRs[i].Data.data());
			cv::resize(tMat, tMat, cv::Size(dispW, dispH));
			int MinIrValue = 0;
			int MaxIrValue = 1000;
			cv::threshold(tMat, tMat, MaxIrValue, MaxIrValue, cv::THRESH_TRUNC);
			cv::threshold(tMat, tMat, MinIrValue, 0, cv::THRESH_TOZERO);
			tMat = (tMat - MinIrValue) * 65535 / (MaxIrValue - MinIrValue);
			tMat.convertTo(tMat, CV_8U, 1.0 / 256.0);
			cv::merge(std::vector<cv::Mat>{tMat, tMat, tMat}, tMat);
			cv::Mat mask;
			cv::inRange(tMat, cv::Scalar(0, 0, 0), cv::Scalar(0, 0, 0), mask);
			tMat.setTo(cv::Scalar(128, 0, 0), mask);
		}
		sprintf_s(Testo, "SN: %s", IRs[i].SerialNumber.c_str());
		cv::putText(tMat, Testo, cv::Point(10, 20), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);
		sprintf_s(Testo, "Device Timestamp: %" PRId64 " us", IRs[i].Timestamp.Device.count());
		cv::putText(tMat, Testo, cv::Point(10, 40), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);
		sprintf_s(Testo, "System Timestamp: %" PRId64 " ns", IRs[i].Timestamp.System.count());
		cv::putText(tMat, Testo, cv::Point(10, 60), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);
		IRMats.push_back(tMat);
	}
	cv::Mat tPlotIR;
	IRMats[0].copyTo(tPlotIR);
	for (int i = 1; i < IRMats.size(); i++)
		vconcat(tPlotIR, IRMats[i], tPlotIR);
	PlotImages.push_back(tPlotIR);
		
	for (int i = 0; i < Depths.size(); i++) {
		cv::Mat tMat;
		if (Depths[i].Width <= 0)
			tMat = cv::Mat::zeros(cv::Size(dispW, dispH), CV_8UC3);
		else {
			tMat = cv::Mat(Depths[i].Height, Depths[i].Width, CV_16UC1, Depths[i].Data.data());
			cv::resize(tMat, tMat, cv::Size(dispW, dispH));
			cv::threshold(tMat, tMat, AK::MAX_RANGE, AK::MAX_RANGE, cv::THRESH_TRUNC);
			cv::threshold(tMat, tMat, AK::MIN_RANGE, 0, cv::THRESH_TOZERO);
			tMat = (tMat - AK::MIN_RANGE) * 65535 / (AK::MAX_RANGE - AK::MIN_RANGE);
			tMat.convertTo(tMat, CV_8U, 1.0 / 256.0);
			cv::applyColorMap(tMat, tMat, cv::COLORMAP_JET);
			cv::Mat mask;
			cv::inRange(tMat, cv::Scalar(128, 0, 0), cv::Scalar(128, 0, 0), mask);
			tMat.setTo(cv::Scalar(0, 0, 0), mask);			
		}
		sprintf_s(Testo, "SN: %s", Depths[i].SerialNumber.c_str());
		cv::putText(tMat, Testo, cv::Point(10, 20), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);
		sprintf_s(Testo, "Device Timestamp: %" PRId64 " us", Depths[i].Timestamp.Device.count());
		cv::putText(tMat, Testo, cv::Point(10, 40), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);
		sprintf_s(Testo, "System Timestamp: %" PRId64 " ns", Depths[i].Timestamp.System.count());
		cv::putText(tMat, Testo, cv::Point(10, 60), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);
		DepthMats.push_back(tMat);
	}
	cv::Mat tPlotDepth;
	DepthMats[0].copyTo(tPlotDepth);
	for (int i = 1; i < DepthMats.size(); i++)
		cv::vconcat(tPlotDepth, DepthMats[i], tPlotDepth);
	PlotImages.push_back(tPlotDepth);
	
		
	/*
	for (int i = 0; i < DepthsA.size(); i++) {
		cv::Mat tMat;
		if (DepthsA[i].Width <= 0)
			tMat = cv::Mat::zeros(cv::Size(dispW, dispH), CV_8UC3);
		else {
			tMat = cv::Mat(DepthsA[i].Height, DepthsA[i].Width, CV_16UC1, DepthsA[i].Data.data());
			cv::threshold(tMat, tMat, AK::MAX_RANGE, AK::MAX_RANGE, cv::THRESH_TRUNC);
			cv::threshold(tMat, tMat, AK::MIN_RANGE, 0, cv::THRESH_TOZERO);
			tMat = (tMat - AK::MIN_RANGE) * 65535 / (AK::MAX_RANGE - AK::MIN_RANGE);
			tMat.convertTo(tMat, CV_8U, 1.0 / 256.0);
			cv::applyColorMap(tMat, tMat, cv::COLORMAP_JET);
			cv::Mat mask;
			cv::inRange(tMat, cv::Scalar(128, 0, 0), cv::Scalar(128, 0, 0), mask);
			tMat.setTo(cv::Scalar(0, 0, 0), mask);
			cv::resize(tMat, tMat, cv::Size(dispW, dispH));
		}
		sprintf_s(Testo, "SN: %s", DepthsA[i].SerialNumber.c_str());
		cv::putText(tMat, Testo, cv::Point(10, 20), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);
		sprintf_s(Testo, "Device Timestamp: %" PRId64 " us", DepthsA[i].Timestamp.Device.count());
		cv::putText(tMat, Testo, cv::Point(10, 40), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);
		sprintf_s(Testo, "System Timestamp: %" PRId64 " ns", DepthsA[i].Timestamp.System.count());
		cv::putText(tMat, Testo, cv::Point(10, 60), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);
		DepthAMats.push_back(tMat);
	}
	cv::Mat tPlotDepthAligned;
	DepthAMats[0].copyTo(tPlotDepthAligned);
	for (int i = 1; i < DepthAMats.size(); i++)
		cv::vconcat(tPlotDepthAligned, DepthAMats[i], tPlotDepthAligned);
	PlotImages.push_back(tPlotDepthAligned);
	*/

	cv::hconcat(PlotImages, PlotImage);
	cv::imshow(m_winName1, PlotImage);
	cv::waitKey(1);
}


void CDisplay::ShowBodyTracking()
{
	std::vector<cv::Mat> PlotBodyIndexMapA;
	for (int i = 0; i < BodyIndexMapsA.size(); i++) {
		cv::Mat tMat;
		if (DrawBodyOnColorImage(i, tMat) == 0)
			PlotBodyIndexMapA.push_back(tMat);
	}
	
	if (PlotBodyIndexMapA.size() > 0) {
		int dispW = (int)(WINW / (int)PlotBodyIndexMapA.size());
		int dispH = (int)(WINH * 1.8);
		char Testo[256];

		for (int i = 0; i < PlotBodyIndexMapA.size(); i++) {
			cv::Mat tMat;
			if (PlotBodyIndexMapA[i].cols == 1)
				tMat = cv::Mat::zeros(cv::Size(dispW, dispH), CV_8UC3);
			else {
				PlotBodyIndexMapA[i].copyTo(tMat);
				cv::resize(tMat, tMat, cv::Size(dispW, dispH));
			}
			sprintf_s(Testo, "SN: %s", BodyIndexMapsA[i].SerialNumber.c_str());
			cv::putText(tMat, Testo, cv::Point(10, 20), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);
			sprintf_s(Testo, "Device Timestamp: %" PRId64 " us", BodyIndexMapsA[i].Timestamp.Device.count());
			cv::putText(tMat, Testo, cv::Point(10, 40), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);
			sprintf_s(Testo, "System Timestamp: %" PRId64 " ns", BodyIndexMapsA[i].Timestamp.System.count());
			cv::putText(tMat, Testo, cv::Point(10, 60), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);
			tMat.copyTo(PlotBodyIndexMapA[i]);
		}
		cv::Mat PlotBIM;
		PlotBodyIndexMapA[0].copyTo(PlotBIM);
		for (int i = 1; i < PlotBodyIndexMapA.size(); i++)
			cv::hconcat(PlotBIM, PlotBodyIndexMapA[i], PlotBIM);

		cv::imshow(m_winName2, PlotBIM);
		cv::waitKey(1);
	}		
}

void CDisplay::SaveCaptures()
{
	// Save color images
	for (int i = 0; i < Colors.size(); i++) {
		cv::Mat imageRGB;
		if (Colors[i].Width > 0) {
			cv::Mat imageRGBraw(1, 4 * Colors[i].Height * Colors[i].Width, CV_8UC1, (void*)Colors[i].Data.data());
			imageRGB = imdecode(imageRGBraw, cv::IMREAD_COLOR);
		}

		char filenameC[256];
		sprintf_s(filenameC, "%s\\SN%s_ID%03d_Color_SystemTimeStamp%" PRId64 "ns.png", captureDestinationFolder.c_str(), Colors[i].SerialNumber.c_str(), iSaveFrame, Colors[i].Timestamp.System.count());
		std::string filenameS(filenameC);
		cv::imwrite(filenameS, imageRGB);
	}

	// Save color aligned images
	for (int i = 0; i < ColorsA.size(); i++) {
		cv::Mat imageRGB;
		cv::Mat dImage(Depths[i].Height, Depths[i].Width, CV_16UC1, Depths[i].Data.data());
		cv::Mat mask;
		cv::inRange(dImage, cv::Scalar(0), cv::Scalar(0), mask);
		imageRGB = cv::Mat(ColorsA[i].Height, ColorsA[i].Width, CV_8UC4, (void*)ColorsA[i].Data.data(), cv::Mat::AUTO_STEP);
		imageRGB.setTo(cv::Scalar(0, 0, 0, 0), mask);
		
		char filenameC[256];
		sprintf_s(filenameC, "%s\\SN%s_ID%03d_ColorAligned_SystemTimeStamp%" PRId64 "ns.png", captureDestinationFolder.c_str(), ColorsA[i].SerialNumber.c_str(), iSaveFrame, ColorsA[i].Timestamp.System.count());
		std::string filenameS(filenameC);
		cv::imwrite(filenameS, imageRGB);
	}

	// Save depth images
	for (int i = 0; i < Depths.size(); i++) {
		cv::Mat imageDepth;
		if (Depths[i].Width > 0)
			imageDepth = cv::Mat(Depths[i].Height, Depths[i].Width, CV_16UC1, (void*)Depths[i].Data.data());

		char filenameC[256];
		sprintf_s(filenameC, "%s\\SN%s_ID%03d_Depth_SystemTimeStamp%" PRId64 "ns.png", captureDestinationFolder.c_str(), Depths[i].SerialNumber.c_str(), iSaveFrame, Depths[i].Timestamp.System.count());
		std::string filenameS(filenameC);
		cv::imwrite(filenameS, imageDepth);
	}

	// Save depth aligned images
	for (int i = 0; i < DepthsA.size(); i++) {
		cv::Mat imageDepth;
		if (DepthsA[i].Width > 0)
			imageDepth = cv::Mat(DepthsA[i].Height, DepthsA[i].Width, CV_16UC1, (void*)DepthsA[i].Data.data());

		char filenameC[256];
		sprintf_s(filenameC, "%s\\SN%s_ID%03d_DepthAligned_SystemTimeStamp%" PRId64 "ns.png", captureDestinationFolder.c_str(), DepthsA[i].SerialNumber.c_str(), iSaveFrame, DepthsA[i].Timestamp.System.count());
		std::string filenameS(filenameC);
		cv::imwrite(filenameS, imageDepth);
	}

	// Save IR images
	for (int i = 0; i < IRs.size(); i++) {
		cv::Mat imageIR;
		if (IRs[i].Width > 0)
			imageIR = cv::Mat(IRs[i].Height, IRs[i].Width, CV_16UC1, (void*)IRs[i].Data.data());

		char filenameC[256];
		sprintf_s(filenameC, "%s\\SN%s_ID%03d_Ir_SystemTimeStamp%" PRId64 "ns.png", captureDestinationFolder.c_str(), IRs[i].SerialNumber.c_str(), iSaveFrame, IRs[i].Timestamp.System.count());
		std::string filenameS(filenameC);
		cv::imwrite(filenameS, imageIR);
	}

	// Save IR aligned images
	for (int i = 0; i < IRsA.size(); i++) {
		cv::Mat imageIR;
		if (IRsA[i].Width > 0)
			imageIR = cv::Mat(IRsA[i].Height, IRsA[i].Width, CV_16UC1, (void*)IRsA[i].Data.data());

		char filenameC[256];
		sprintf_s(filenameC, "%s\\SN%s_ID%03d_IrAligned_SystemTimeStamp%" PRId64 "ns.png", captureDestinationFolder.c_str(), IRsA[i].SerialNumber.c_str(), iSaveFrame, IRsA[i].Timestamp.System.count());
		std::string filenameS(filenameC);
		cv::imwrite(filenameS, imageIR);
	}

	// Save point clouds in color geometry
	for (int i = 0; i < PointcloudsColor.size(); i++) {
		if (PointcloudsColor[i].Points.size() > 0) {

			// Set the name of the ply file (destination is the path)
			char filenameC[256];
			sprintf_s(filenameC, "%s\\SN%s_ID%03d_PointcloudColor_SystemTimeStamp%" PRId64 "ns.ply", captureDestinationFolder.c_str(), PointcloudsColor[i].SerialNumber.c_str(), iSaveFrame, PointcloudsColor[i].Timestamp.System.count());

			// Open file for writing
			std::ofstream ofs;
			ofs.open(filenameC, std::ofstream::out | std::ofstream::app);

			if (ofs.is_open()) {
				ofs << "ply" << std::endl;
				ofs << "format binary_little_endian 1.0" << std::endl;
				ofs << "element vertex" << " " << PointcloudsColor[i].Points.size() << std::endl;
				ofs << "property float x" << std::endl;
				ofs << "property float y" << std::endl;
				ofs << "property float z" << std::endl;
				ofs << "property uchar blue" << std::endl;
				ofs << "property uchar green" << std::endl;
				ofs << "property uchar red" << std::endl;
				ofs << "end_header" << std::endl;
			}
			ofs.close();

			std::ofstream ofsbin;
			ofsbin.open(filenameC, std::ofstream::out | std::ofstream::app | std::ofstream::binary);
			if (ofsbin.is_open()) {
				for (int j = 0; j < PointcloudsColor[i].Points.size(); j++) {
					ofsbin.write(reinterpret_cast<char*>(&PointcloudsColor[i].Points[j].x), sizeof(float));
					ofsbin.write(reinterpret_cast<char*>(&PointcloudsColor[i].Points[j].y), sizeof(float));
					ofsbin.write(reinterpret_cast<char*>(&PointcloudsColor[i].Points[j].z), sizeof(float));
					ofsbin.write(reinterpret_cast<char*>(&PointcloudsColor[i].Points[j].b), sizeof(uint8_t));
					ofsbin.write(reinterpret_cast<char*>(&PointcloudsColor[i].Points[j].g), sizeof(uint8_t));
					ofsbin.write(reinterpret_cast<char*>(&PointcloudsColor[i].Points[j].r), sizeof(uint8_t));
				}
				ofsbin.close();
			}
		}
	}

	// Save point clouds in depth geometry
	for (int i = 0; i < PointcloudsDepth.size(); i++) {
		if (PointcloudsDepth[i].Points.size() > 0) {

			// Set the name of the ply file (destination is the path)
			char filenameC[256];
			sprintf_s(filenameC, "%s\\SN%s_ID%03d_PointcloudDepth_SystemTimeStamp%" PRId64 "ns.ply", captureDestinationFolder.c_str(), PointcloudsDepth[i].SerialNumber.c_str(), iSaveFrame, PointcloudsDepth[i].Timestamp.System.count());

			// Open file for writing
			std::ofstream ofs;
			ofs.open(filenameC, std::ofstream::out | std::ofstream::app);

			if (ofs.is_open()) {
				ofs << "ply" << std::endl;
				ofs << "format binary_little_endian 1.0" << std::endl;
				ofs << "element vertex" << " " << PointcloudsDepth[i].Points.size() << std::endl;
				ofs << "property float x" << std::endl;
				ofs << "property float y" << std::endl;
				ofs << "property float z" << std::endl;
				ofs << "property uchar blue" << std::endl;
				ofs << "property uchar green" << std::endl;
				ofs << "property uchar red" << std::endl;
				ofs << "end_header" << std::endl;
			}
			ofs.close();

			std::ofstream ofsbin;
			ofsbin.open(filenameC, std::ofstream::out | std::ofstream::app | std::ofstream::binary);
			if (ofsbin.is_open()) {
				for (int j = 0; j < PointcloudsDepth[i].Points.size(); j++) {
					ofsbin.write(reinterpret_cast<char*>(&PointcloudsDepth[i].Points[j].x), sizeof(float));
					ofsbin.write(reinterpret_cast<char*>(&PointcloudsDepth[i].Points[j].y), sizeof(float));
					ofsbin.write(reinterpret_cast<char*>(&PointcloudsDepth[i].Points[j].z), sizeof(float));
					ofsbin.write(reinterpret_cast<char*>(&PointcloudsDepth[i].Points[j].b), sizeof(uint8_t));
					ofsbin.write(reinterpret_cast<char*>(&PointcloudsDepth[i].Points[j].g), sizeof(uint8_t));
					ofsbin.write(reinterpret_cast<char*>(&PointcloudsDepth[i].Points[j].r), sizeof(uint8_t));
				}
				ofsbin.close();
			}
		}
	}

}


int CDisplay::DrawBodyOnColorImage(int idxBodyFrame, cv::Mat &Image)
{
	std::vector<cv::Mat> ColorMats, BIMaMats;
	for (int i = 0; i < Colors.size(); i++) {
		cv::Mat tColorMat;
		if (Colors[i].Width <= 0)
			tColorMat = cv::Mat::zeros(cv::Size(1, 1), CV_8UC3);
		else {
			cv::Mat rawColorMat(1, 4 * Colors[i].Height * Colors[i].Width, CV_8UC1, (void*)Colors[i].Data.data());
			tColorMat = imdecode(rawColorMat, cv::IMREAD_COLOR);
		}			
		ColorMats.push_back(tColorMat);
	}
	for (int i = 0; i < BodyIndexMapsA.size(); i++) {
		cv::Mat tBIMAMat(BodyIndexMapsA[i].Height, BodyIndexMapsA[i].Width, CV_8UC1, BodyIndexMapsA[i].Data.data());
		BIMaMats.push_back(tBIMAMat);
	}

	int nRet = 0;
	int idxColor = FindCorrespondingIdx(Colors, BodyIndexMapsA[idxBodyFrame].SerialNumber);
	int idxBodyID = FindCorrespondingIdx(BodyIDs, BodyIndexMapsA[idxBodyFrame].SerialNumber);
	
	// Retrieve the only skeletons of the reference camera
	std::vector<AK::SSkeleton> tSkels;
	for (int i = 0; i < Skeletons.size(); i++) {
		if (Skeletons[i].SerialNumber == BodyIndexMapsA[idxBodyFrame].SerialNumber)
			tSkels.push_back(Skeletons[i]);
	}
	
	if (ColorMats[idxColor].cols > 1 && idxColor >= 0 && idxBodyID >= 0) {
		ColorMats[idxColor].copyTo(Image);
		cv::Mat BIMa;
		BIMaMats[idxBodyFrame].copyTo(BIMa);
		uint8_t * pxl;

		for (int row = 0; row < BIMa.rows; row++) {
			pxl = BIMa.ptr<uint8_t>(row);
			for (int col = 0; col < BIMa.cols; col++) {
				if (pxl[col] != K4ABT_BODY_INDEX_MAP_BACKGROUND) {
					uint32_t bodyId = BodyIDs[idxBodyID].BodyIndexToBodyID[pxl[col]];
					Image.ptr<cv::Vec3b>(row)[col][0] = g_bodyColors[bodyId % g_bodyColors.size()].b;
					Image.ptr<cv::Vec3b>(row)[col][1] = g_bodyColors[bodyId % g_bodyColors.size()].g;
					Image.ptr<cv::Vec3b>(row)[col][2] = g_bodyColors[bodyId % g_bodyColors.size()].r;
				}
			}
		}

		for (int s = 0; s < tSkels.size(); s++) {
			for (int j = 0; j < (int)K4ABT_JOINT_COUNT; j++) {
				cv::Scalar jointColor = cv::Scalar(0.8 * g_bodyColors[tSkels[s].BodyID % g_bodyColors.size()].b,
					0.8 * g_bodyColors[tSkels[s].BodyID % g_bodyColors.size()].g,
					0.8 * g_bodyColors[tSkels[s].BodyID % g_bodyColors.size()].r);
				cv::Point2f currJ2DColorSpace(tSkels[s].Joint2DColorSpace[j].x, tSkels[s].Joint2DColorSpace[j].y);
				cv::circle(Image, currJ2DColorSpace, (int)(20.f * tSkels[s].Confidence[j]), jointColor, cv::FILLED);
			}
		}
	}
	else
		nRet = -1;

	return nRet;

}


void CDisplay::SendImuDataToDlg()
{
	EmptyImuDataInDlg();
	for (int i = 0; i < IMUs.size(); i++) {
		CString tString;
		tString.Format(L"--------------------------- KINECT AZURE NO. %d ---------------------------\r\n", i);
		AppendImuDataToDlg(tString);
		tString.Format(L"Sensor Temperature: %f�C\r\nTimestamp: %" PRIu64" \xB5s\r\n", IMUs[i].Temperature, IMUs[i].Accelerometer.Timestamp);
		AppendImuDataToDlg(tString);
		tString.Format(L"Acceleration data: ( %f , %f , %f ) [m/s^2]\r\n", 
			IMUs[i].Accelerometer.X, IMUs[i].Accelerometer.Y, IMUs[i].Accelerometer.Z);
		AppendImuDataToDlg(tString);
		tString.Format(L"Gyroscope data: ( %f , %f , %f ) [m/s^2]\r\n",
			IMUs[i].Gyroscope.X, IMUs[i].Gyroscope.Y, IMUs[i].Gyroscope.Z);
		AppendImuDataToDlg(tString);
	}
}


void CDisplay::AppendImuDataToDlg(LPCTSTR Message)
{
	CAzureKinectDlg *pDlg = (CAzureKinectDlg *)p_Container;

	// get the initial text length
	int nLength = pDlg->m_editImu.GetWindowTextLength();
	// put the selection at the end of text
	pDlg->m_editImu.SetSel(nLength, nLength);
	// replace the selection
	pDlg->m_editImu.ReplaceSel(Message);
}


void CDisplay::EmptyImuDataInDlg()
{
	CAzureKinectDlg *pDlg = (CAzureKinectDlg *)p_Container;
	pDlg->m_editImu.SetWindowText(L"");
}


BOOL CDisplay::CCountWindows::StaticWndEnumProc(HWND hwnd, LPARAM lParam)
{
	CCountWindows *pThis = reinterpret_cast<CCountWindows*>(lParam);
	return pThis->WndEnumProc(hwnd);
}


BOOL CDisplay::CCountWindows::WndEnumProc(HWND hwnd)
{
	m_count++;
	hWindows.push_back(hwnd);

	int length = GetWindowTextLength(hwnd);
	CString windowTitle;

	if (length > 0) {
		TCHAR* buffer;
		buffer = new TCHAR[length + 1];
		memset(buffer, 0, (length + 1) * sizeof(TCHAR));

		GetWindowText(hwnd, buffer, length + 1);
		windowTitle = buffer;
		delete[] buffer;
	}
	else
		windowTitle = L"";
	WinNames.push_back(windowTitle);

	return TRUE;
}


int CDisplay::CCountWindows::CountThem()
{
	m_count = 0;
	hWindows.clear();
	WinNames.clear();
	EnumWindows(StaticWndEnumProc, reinterpret_cast<LPARAM>(this));
	return m_count;
}