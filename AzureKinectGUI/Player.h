#pragma once

#include "AzureKinect.h"
#include "Display.h"
#include "Converter.h"
#include <filesystem>

class CPlayer
{
public:
	CPlayer();
	~CPlayer();

	size_t InitFileNames();
	void InitPlaybacks();
	void DisplayFramesAt(int i);
	size_t InitConverters(const std::string);
	bool CheckConversion(conversion_type inputConversion);
	void AddConverter(conversion_type inputConversion);
	void ConvertFramesAt(int pos);
	void ClosePlaybacks(bool withBodyTracking);

public:
	CDisplay Display;
	CString m_BaseFilePathAndName;

	CAzureKinect VirtualKinects;
	std::vector<std::string> FileNames;
	std::vector<std::shared_ptr<CConverter>> Converters;
	std::vector<std::string> SerialNumbers;
	std::string InputPath, RefCamFileName, BaseFileName;
	std::string OutputBaseDir;
	int nFramesMin, m_framePos;
	bool b_isProcessing, b_stopConversion, b_doBodyTracking, b_isDoBodyTrackingChanged, b_isFirstFrame;
	bool b_convertSkeletons, b_convertPointcloudColor, b_convertPointcloudDepthColor;
	bool b_convertPngColor, b_convertPngColorAligned, b_convertPngDepth, b_convertPngDepthAligned, b_convertPngIr, b_convertPngIrAligned;
	bool b_convertRawColor, b_convertRawColorAligned, b_convertRawDepth, b_convertRawDepthAligned, b_convertRawIr, b_convertRawIrAligned;
};

