// Controller.cpp : implementation file
//

#include "stdafx.h"
#include "Controller.h"


// CController

IMPLEMENT_DYNCREATE(CController, CWinThread)

CController::CController()
	: b_acqLoop(false)
	, b_setExposuresNow(false)
	, b_startRecording(false)
	, b_stopRecording(false)
	, b_isRecording(false)
	, b_existThread(false)
	, b_doBodyTracking(false)
	, b_isDoBodyTrackingChanged(false)
	, b_takeSnapshot(false)
{
	m_bAutoDelete = false;
}

CController::~CController()
{
}

BOOL CController::InitInstance()
{
	b_existThread = true;
	nKinects = Kinects.Open();
	if (nKinects > 0) 
		return TRUE;
	else 	
		return FALSE;	
}

int CController::ExitInstance()
{
	if (nKinects > 0)
		Kinects.Close();

	DWORD exit_code = NULL;
	if (this != NULL) {
		b_existThread = false;
		GetExitCodeThread(m_hThread, &exit_code);
		if (exit_code == STILL_ACTIVE) {
			WaitForSingleObject(m_hThread, 1000);
			CloseHandle(m_hThread);
		}
		m_hThread = NULL;
	}
	b_isInitThreadEnded = true;
	
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CController, CWinThread)
END_MESSAGE_MAP()


// CController message handlers


BOOL CController::Run()
{
	b_isInitThreadEnded = true;
	bool isFirstCapture = true;

	// Set Kinect configuration
	Kinects.SetConfiguration();
	
	// Start acquisition
	nKinects = Kinects.StartAcquisitions();
	Kinects.SetExposures(exposure);
	b_setExposuresNow = false;
	
	// Open image window and set to GPU/CPU mode
	Display.nImageMax = 2 * nKinects;
	Display.Init(nKinects == 1 && b_doBodyTracking);

	while (b_acqLoop && nKinects > 0) {
		
		if (isFirstCapture) {
			if (!Kinects.WaitForFirstCapture())
				break;
			b_isDoBodyTrackingChanged = false;
			isFirstCapture = false;
		}
		
		if (b_startRecording) {
			Display.Close(b_doBodyTracking);
			std::wstring wstr(m_basePathAndName);
			const std::string str(wstr.begin(), wstr.end());
			b_isRecording = Kinects.StartRecordings(str);
			b_startRecording = false;
		}
		else if (b_stopRecording) {
			Kinects.StopRecordings();
			Display.Init(nKinects == 1 && b_doBodyTracking);
			b_stopRecording = false;
			b_isRecording = false;
		}
		
		if (b_setExposuresNow) {
			Kinects.SetExposures(exposure);
			b_setExposuresNow = false;
		}

		Kinects.GetIMUSamples(1);
		Kinects.GetCaptures(1);
				
		if (!b_isRecording) {			
			if (nKinects == 1 && b_doBodyTracking)
				Kinects.DoBodyTracking();
			
			// Retrieve and assign data
			Kinects.GetImages(Display.Colors, AK::COLOR);
			Kinects.GetImages(Display.IRs, AK::IR);
			Kinects.GetImages(Display.Depths, AK::DEPTH);

			if (nKinects == 1 && b_doBodyTracking) {
				Kinects.GetBodyIndexMaps(Display.BodyIndexMapsA, AK::BODYINDEXMAPA);
				Kinects.GetBodyIDs(Display.BodyIDs);
				Kinects.GetSkeletons(Display.Skeletons);
			}			
			Kinects.GetIMUs(Display.IMUs);

			if (!b_takeSnapshot) {
				Display.SendImuDataToDlg();
				if (Display.Colors.size() > 0) {
					Display.ShowCaptures();
					if (nKinects == 1 && b_doBodyTracking)
						Display.ShowBodyTracking();
				}
			}
			else {
				if (Display.Colors.size() == 0) {
					Kinects.GetImages(Display.Colors, AK::COLOR);
					Kinects.GetImages(Display.Depths, AK::DEPTH);
					Kinects.GetImages(Display.IRs, AK::IR);
				}
				if (Display.Colors.size() > 0) {
					Kinects.GetImages(Display.ColorsA, AK::COLORA);
					Kinects.GetImages(Display.DepthsA, AK::DEPTHA);
					Kinects.GetImages(Display.IRsA, AK::IRA);
					Kinects.GetPointcloudsOnColorGeometry(Display.PointcloudsColor);
					Kinects.GetPointcloudsOnDepthGeometry(Display.PointcloudsDepth);

					Display.SaveCaptures();
					Display.iSaveFrame++;
					b_takeSnapshot = false;
				}					
			}			
			Display.ClearVariables();

			Kinects.ReleaseBodyFrames();
		}
		
		Kinects.ReleaseIMUSamples();
		Kinects.ReleaseCaptures();

		if (b_isDoBodyTrackingChanged) {
			Display.ReInit(nKinects == 1 && b_doBodyTracking);
			b_isDoBodyTrackingChanged = false;
		}

	}

	if (b_isRecording) {
		Kinects.StopRecordings();
		b_isRecording = false;
	}
		
	// Close image window
	Display.Close(b_doBodyTracking);
	
	// Stop acquisition
	Kinects.StopAcquisitions();
	
	// Exit thread
	ExitInstance();

	if (nKinects == 0) {
		AfxMessageBox(L"Invalid number of opened kinects\r\nPlease stop and restart after checking:\r\n\t1. A device is connected\r\n\t2. Sync cables are plugged\r\n\t3. Options are correctly assigned");
		return FALSE;
	}
	
	return TRUE;
}