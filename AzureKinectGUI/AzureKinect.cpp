#include "stdafx.h"
#include "AzureKinect.h"
#include "AzureKinectGUIDlg.h"


CAzureKinect::CAzureKinect()
	: m_fps(30)
	, i_extSync(0)
{
}


CAzureKinect::~CAzureKinect()
{	
}

int CAzureKinect::Open()
{
	Close();	

	uint32_t device_count = k4a_device_get_installed_count();
	int nKinects = 0;	
	
	for (uint8_t deviceIndex = 0; deviceIndex < device_count; deviceIndex++)
	{
		try {
			std::shared_ptr<MyKinect> tMyKinect;
			tMyKinect = std::make_shared<MyKinect>();
			tMyKinect->Device.Worker = NULL;
			tMyKinect->Recorder.Worker = NULL;
			tMyKinect->Player.Worker = NULL;
			tMyKinect->Recorder.b_recordImu = true;
			tMyKinect->Tracker = NULL;
			
			// Open device and push it back if opened
			tMyKinect->Device.Worker = k4a::device::open(deviceIndex);
			tMyKinect->SerialNumber = tMyKinect->Device.Worker.get_serialnum();
			m_kinects.push_back(tMyKinect);
			nKinects++;

			CString tString;
			tString.Format(L"Open Azure Kinect %d (S/N %s)\r\n", deviceIndex, GetWChar(tMyKinect->SerialNumber.c_str()));
			AppendLogMessages(tString);
		}
		catch (const std::exception& e)
		{
			CString tString;
			tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
			AppendLogMessages(tString);
			continue;
		}		
	}
	return nKinects;
}

int CAzureKinect::Open(std::vector<std::string> PlaybackFilenames)
{
	Close();

	int nPlaybacks = 0;
	size_t nInPlaybacks = PlaybackFilenames.size();
	for (size_t i = 0; i < nInPlaybacks; i++) {
		try {
			std::shared_ptr<MyKinect> tMyKinect;
			tMyKinect = std::make_shared<MyKinect>();
			tMyKinect->Device.Worker = NULL;
			tMyKinect->Recorder.Worker = NULL;
			tMyKinect->Player.Worker = NULL;
			tMyKinect->Tracker = NULL;
			
			// Open device and push it back if opened
			strcpy_s(tMyKinect->Player.Filename, 256, PlaybackFilenames[i].c_str());
			tMyKinect->Player.Worker = k4a::playback::open(tMyKinect->Player.Filename);
						
			tMyKinect->Player.Configuration = tMyKinect->Player.Worker.get_record_configuration();
			switch (tMyKinect->Player.Configuration.camera_fps) {
			case K4A_FRAMES_PER_SECOND_5:
				tMyKinect->Player.Fps = 5;
				break;
			case K4A_FRAMES_PER_SECOND_15:
				tMyKinect->Player.Fps = 15;
				break;
			case K4A_FRAMES_PER_SECOND_30:
				tMyKinect->Player.Fps = 30;
				break;
			}
			tMyKinect->Player.Duration = tMyKinect->Player.Worker.get_recording_length();
			tMyKinect->Player.NFrames = static_cast<int>(tMyKinect->Player.Duration.count() * (unsigned long long)tMyKinect->Player.Fps / 1000000ull);

			// Get serialnumber
			tMyKinect->Player.Worker.get_tag("K4A_DEVICE_SERIAL_NUMBER", &tMyKinect->SerialNumber);

			// Configure Calibration, Transformation and Tracker with default configuration (K4ABT_TRACKER_CONFIG_DEFAULT)
			tMyKinect->Calibration = tMyKinect->Player.Worker.get_calibration();
			tMyKinect->Transformation = k4a_transformation_create(&tMyKinect->Calibration);
			k4abt_tracker_configuration_t tracker_config = K4ABT_TRACKER_CONFIG_DEFAULT;
			tracker_config.processing_mode = K4ABT_TRACKER_PROCESSING_MODE_GPU_CUDA;
			tMyKinect->Tracker = k4abt::tracker::create(tMyKinect->Calibration, tracker_config);
			
			// Push device back
			m_kinects.push_back(tMyKinect);
			nPlaybacks++;

			CString tString;
			tString.Format(L"Open playback object %d. Virtual Azure Kinect (S/N %s) initialized\r\n", i, GetWChar(tMyKinect->SerialNumber.c_str()));
			AppendLogMessages(tString);
		}
		catch (const std::exception& e)
		{
			CString tString;
			tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
			AppendLogMessages(tString);
			tString.Format(L"If unable to create k4abt tracker, try to add the path of the Azure Kinect Body Tracking library \r\n (e.g. ""C:\\Program Files\\Azure Kinect Body Tracking SDK\\tools"") to the environmental variable PATH\r\nThis module works with Body Tracking SDK version v1.1.2!\r\n");
			AppendLogMessages(tString);
			continue;
		}
	}
	return nPlaybacks;
}


void CAzureKinect::CloseSingleKinect(int i)
{
	CString tString;
	tString.Format(L"Close Azure Kinect %d (S/N %s)\r\n", i, GetWChar(m_kinects[i]->SerialNumber.c_str()));
	AppendLogMessages(tString);

	if (m_kinects[i]->Device.Worker) {
		m_kinects[i]->Device.Worker.close();
		m_kinects[i]->Device.Worker = NULL;
	}
	if (m_kinects[i]->Recorder.Worker) {
		m_kinects[i]->Recorder.Worker.close();
		m_kinects[i]->Recorder.Worker = NULL;
	}
	if (m_kinects[i]->Player.Worker) {
		m_kinects[i]->Player.Worker.close();
		m_kinects[i]->Player.Worker = NULL;
	}
	m_kinects[i]->Tracker = NULL;
	m_kinects[i]->Transformation = NULL;

	m_kinects.erase(m_kinects.begin() + i);
}


int CAzureKinect::Close()
{
	int i = 0;
	for (auto &kinect : m_kinects) {
		CString tString;
		tString.Format(L"Close Azure Kinect %d (S/N %s)\r\n", i, GetWChar(kinect->SerialNumber.c_str()));
		AppendLogMessages(tString);
		i++;
		
		if (kinect->Device.Worker) {
			kinect->Device.Worker.close();
			kinect->Device.Worker = NULL;
			kinect->Tracker = NULL;
			kinect->Transformation = NULL;
		}
		if (kinect->Recorder.Worker) {
			kinect->Recorder.Worker.close();
			kinect->Recorder.Worker = NULL;
		}
		if (kinect->Player.Worker) {
			kinect->Player.Worker.close();
			kinect->Player.Worker = NULL;
		}
	}
	m_kinects.clear();

	ReleaseCaptures();
	ReleaseBodyFrames();
	ReleaseIMUSamples();
	return 0;
}

size_t CAzureKinect::Count()
{
	return m_kinects.size();
}


int CAzureKinect::SetConfiguration()
{
	k4a_fps_t fps;
	k4a_color_resolution_t colorRes;
	k4a_depth_mode_t depthMode;

	switch (m_inputConfig) {
	case AK::COLOR43_2048x1536_DEPTHN_640x576_30FPS:
		fps = K4A_FRAMES_PER_SECOND_30;
		colorRes = K4A_COLOR_RESOLUTION_1536P;
		depthMode = K4A_DEPTH_MODE_NFOV_UNBINNED;
		m_fps = 30;
		break;
	case AK::COLOR43_4096x3072_DEPTHN_640x576_15FPS:
		fps = K4A_FRAMES_PER_SECOND_15;
		colorRes = K4A_COLOR_RESOLUTION_3072P;
		depthMode = K4A_DEPTH_MODE_NFOV_UNBINNED;
		m_fps = 15;
		break;
	case AK::COLOR43_2048x1536_DEPTHW_512x512_30FPS:
		fps = K4A_FRAMES_PER_SECOND_30;
		colorRes = K4A_COLOR_RESOLUTION_1536P;
		depthMode = K4A_DEPTH_MODE_WFOV_2X2BINNED;
		m_fps = 30;
		break;
	case AK::COLOR43_4096x3072_DEPTHW_1024x1024_15FPS:
		fps = K4A_FRAMES_PER_SECOND_15;
		colorRes = K4A_COLOR_RESOLUTION_3072P;
		depthMode = K4A_DEPTH_MODE_WFOV_UNBINNED;
		m_fps = 15;
		break;
	case AK::COLOR169_QHD_DEPTHN_640x576_30FPS:
		fps = K4A_FRAMES_PER_SECOND_30;
		colorRes = K4A_COLOR_RESOLUTION_1440P;
		depthMode = K4A_DEPTH_MODE_NFOV_UNBINNED;
		m_fps = 30;
		break;
	case AK::COLOR169_4K_DEPTHN_640x576_15FPS:
		fps = K4A_FRAMES_PER_SECOND_15;
		colorRes = K4A_COLOR_RESOLUTION_2160P;
		depthMode = K4A_DEPTH_MODE_NFOV_UNBINNED;
		m_fps = 15;
		break;
	case AK::COLOR169_QHD_DEPTHW_512x512_30FPS:
		fps = K4A_FRAMES_PER_SECOND_30;
		colorRes = K4A_COLOR_RESOLUTION_1440P;
		depthMode = K4A_DEPTH_MODE_WFOV_2X2BINNED;
		m_fps = 30;
		break;
	case AK::COLOR169_4K_DEPTHW_1024x1024_15FPS:
		fps = K4A_FRAMES_PER_SECOND_15;
		colorRes = K4A_COLOR_RESOLUTION_2160P;
		depthMode = K4A_DEPTH_MODE_WFOV_UNBINNED;
		m_fps = 15;
		break;
	case AK::COLOR169_FHD_DEPTHN_640x576_30FPS:
		fps = K4A_FRAMES_PER_SECOND_30;
		colorRes = K4A_COLOR_RESOLUTION_1080P;
		depthMode = K4A_DEPTH_MODE_NFOV_UNBINNED;
		m_fps = 30;
		break;
	case AK::COLOR169_720P_DEPTHN_640x576_30FPS:
		fps = K4A_FRAMES_PER_SECOND_30;
		colorRes = K4A_COLOR_RESOLUTION_720P;
		depthMode = K4A_DEPTH_MODE_NFOV_UNBINNED;
		m_fps = 30;
		break;
	}
	
	try {
		size_t nKinects = m_kinects.size();
		for (int i = 0; i < nKinects; i++) {
			CString tString;
			tString.Format(L"Set configuration of Azure Kinect %d (S/N %s)\r\n", i, GetWChar(m_kinects[i]->SerialNumber.c_str()));
			AppendLogMessages(tString);

			// Configure the streams
			m_kinects[i]->Device.Configuration = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
			m_kinects[i]->Device.Configuration.camera_fps = fps;
			m_kinects[i]->Device.Configuration.color_format = K4A_IMAGE_FORMAT_COLOR_MJPG;
			m_kinects[i]->Device.Configuration.color_resolution = colorRes;
			m_kinects[i]->Device.Configuration.depth_mode = depthMode;
			m_kinects[i]->Device.Configuration.synchronized_images_only = true; // If set to true, k4a_capture_t objects will only be produced with both color and depth images. If set to false, k4a_capture_t objects may be produced only a single image when the corresponding image is dropped.
			
			if (i_extSync == 1) { // Set of Kinects with local master
				if (i == nKinects - 1) { // The last kinect of the vector is the Master by default
					if (m_kinects[i]->Device.Worker.is_sync_out_connected()) {
						m_kinects[i]->Device.Configuration.wired_sync_mode = K4A_WIRED_SYNC_MODE_MASTER;
						CString tString;
						tString.Format(L"Microsoft Azure Kinect with S/N %s is set as MASTER\r\n", GetWChar(m_kinects[i]->SerialNumber.c_str()));
						AppendLogMessages(tString);
					}
					else {
						m_kinects[i]->Device.Configuration.wired_sync_mode = K4A_WIRED_SYNC_MODE_STANDALONE;
						CString tString;
						tString.Format(L"WARNING: Impossible to set the Azure Kinect with S/N %s as MASTER (Sync-out wire not connected!)\r\nSetting it as STANDALONE camera\r\n", GetWChar(m_kinects[i]->SerialNumber.c_str()));
						AppendLogMessages(tString);
					}						
				}
				else { // The other kinects of the vector are Subordinate by default
					if (m_kinects[i]->Device.Worker.is_sync_in_connected()) {
						m_kinects[i]->Device.Configuration.wired_sync_mode = K4A_WIRED_SYNC_MODE_SUBORDINATE;
						CString tString;
						tString.Format(L"Microsoft Azure Kinect with S/N %s is set as SUBORDINATE\r\n", GetWChar(m_kinects[i]->SerialNumber.c_str()));
						AppendLogMessages(tString);
					}
					else {
						m_kinects[i]->Device.Configuration.wired_sync_mode = K4A_WIRED_SYNC_MODE_STANDALONE;
						CString tString;
						tString.Format(L"WARNING: Impossible to set the Azure Kinect with S/N %s as SUBORDINATE (Sync-in wire not connected!)\r\nSetting it as STANDALONE camera\r\n", GetWChar(m_kinects[i]->SerialNumber.c_str()));
						AppendLogMessages(tString);
					}
				}
			}
			else if (i_extSync == 2) { // Set of Kinects without local master. All cameras are subordinate if sync wires are connected. The master is remote
				// this is valid ONLY with ROS implementations!
				if (m_kinects[i]->Device.Worker.is_sync_in_connected()) {
					m_kinects[i]->Device.Configuration.wired_sync_mode = K4A_WIRED_SYNC_MODE_SUBORDINATE;
					CString tString;
					tString.Format(L"Microsoft Azure Kinect with S/N %s is set as SUBORDINATE\r\n", GetWChar(m_kinects[i]->SerialNumber.c_str()));
					AppendLogMessages(tString);
				}
				else {
					m_kinects[i]->Device.Configuration.wired_sync_mode = K4A_WIRED_SYNC_MODE_STANDALONE;
					CString tString;
					tString.Format(L"WARNING: Impossible to set the Azure Kinect with S/N %s as SUBORDINATE (Sync-in wire not connected!)\r\nSetting it as STANDALONE camera\r\n", GetWChar(m_kinects[i]->SerialNumber.c_str()));
					AppendLogMessages(tString);
				}
			}
			else // Set of standalone cameras
				m_kinects[i]->Device.Configuration.wired_sync_mode = K4A_WIRED_SYNC_MODE_STANDALONE;

		}
		return 0;
	}
	catch (const std::exception& e)
	{
		CString tString;
		tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
		AppendLogMessages(tString);
		return -1;
	}	
}


size_t CAzureKinect::StartAcquisitions()
{	
	size_t nKinects = m_kinects.size();
	
	for (int i = 0; i < nKinects; i++) {
		CString tString;
		tString.Format(L"Start acquisition from Azure Kinect %d (S/N %s)\r\n", i, GetWChar(m_kinects[i]->SerialNumber.c_str()));
		AppendLogMessages(tString);
		try {			
			// Get Calibrtation info for depth alignment
			m_kinects[i]->Calibration = m_kinects[i]->Device.Worker.get_calibration(m_kinects[i]->Device.Configuration.depth_mode, m_kinects[i]->Device.Configuration.color_resolution);
			
			// Create proper transformation
			m_kinects[i]->Transformation = k4a_transformation_create(&m_kinects[i]->Calibration);
			
			// Start streaming
			m_kinects[i]->Device.Worker.start_cameras(&m_kinects[i]->Device.Configuration);
			m_kinects[i]->Device.Worker.start_imu();
			
			// Create body tracker with default configuration (K4ABT_TRACKER_CONFIG_DEFAULT)
			k4abt_tracker_configuration_t tracker_config = K4ABT_TRACKER_CONFIG_DEFAULT;
			tracker_config.processing_mode = K4ABT_TRACKER_PROCESSING_MODE_GPU_CUDA;
			m_kinects[i]->Tracker = k4abt::tracker::create(m_kinects[i]->Calibration, tracker_config);
		}
		catch (const std::exception& e)
		{
			CloseSingleKinect(i);
			CString tString;
			tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
			AppendLogMessages(tString);
			tString.Format(L"If unable to create k4abt tracker, try to add the path of the Azure Kinect Body Tracking library \r\n (e.g. ""C:\\Program Files\\Azure Kinect Body Tracking SDK\\tools"") to the environmental variable PATH\r\nThis module works with Body Tracking SDK version v1.1.2!\r\n");
			AppendLogMessages(tString);
			continue;
		}
	}

	return m_kinects.size();	
}


bool CAzureKinect::StartRecordings(std::string BaseName)
{
	size_t nKinects = m_kinects.size();
	int Count = 0;
	for (int i = 0; i < nKinects; i++) {
		sprintf_s(m_kinects[i]->Recorder.Filename, "%s_MSAzureKinect_SN%s.mkv", BaseName.c_str(), m_kinects[i]->SerialNumber.c_str());
		CString tString;
		tString.Format(L"Start recording from Azure Kinect %d (S/N %s) to\r\n%s\r\n", i, GetWChar(m_kinects[i]->SerialNumber.c_str()), GetWChar(m_kinects[i]->Recorder.Filename));
		AppendLogMessages(tString);
		try {
			m_kinects[i]->Recorder.Worker = k4a::record::create(m_kinects[i]->Recorder.Filename, m_kinects[i]->Device.Worker, m_kinects[i]->Device.Configuration);
			if (m_kinects[i]->Recorder.b_recordImu)
				m_kinects[i]->Recorder.Worker.add_imu_track();
			m_kinects[i]->Recorder.Worker.write_header();
			Count++;
		}
		catch (const std::exception& e)
		{
			CString tString;
			tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
			AppendLogMessages(tString);
			continue;
		}
	}
	return Count == nKinects;
}


void CAzureKinect::GetRecordingsNames(std::vector<std::string>& Names)
{
	Names.clear();
	size_t nKinects = m_kinects.size();
	for (int i = 0; i < nKinects; i++) {
		char tName[256];
		strcpy_s(tName, 256, m_kinects[i]->Recorder.Filename);
		Names.push_back(tName);
	}
}


void CAzureKinect::SetExposures(int inExp)
{	
	//When starting up devices using the multi device synchronization feature, it is highly recommended
	//to do so using a fixed exposure setting. With a manual exposure set, it can take up to eight captures 
	//from the device before images and framerate stabilize. With auto exposure, it can take up to 20 
	//captures before images and framerate stabilize. Use k4a_device_set_color_control()
	size_t nKinects = m_kinects.size();
	for (int i = 0; i < nKinects; i++) {
		// Set exposure
		try{
			if (inExp > 0) {
				CString tString;
				tString.Format(L"Set exposure of Azure Kinect %d (S/N %s) to %d\r\n", i, GetWChar(m_kinects[i]->SerialNumber.c_str()), inExp);
				AppendLogMessages(tString);
				m_kinects[i]->Device.Worker.set_color_control(K4A_COLOR_CONTROL_EXPOSURE_TIME_ABSOLUTE, K4A_COLOR_CONTROL_MODE_MANUAL, inExp);
			}
				
			else {
				CString tString;
				tString.Format(L"Set exposure of Azure Kinect %d (S/N %s) to AUTO\r\n", i, GetWChar(m_kinects[i]->SerialNumber.c_str()));
				AppendLogMessages(tString);
				m_kinects[i]->Device.Worker.set_color_control(K4A_COLOR_CONTROL_EXPOSURE_TIME_ABSOLUTE, K4A_COLOR_CONTROL_MODE_AUTO, 0);
			}
				
		}
		catch (const std::exception& e)
		{
			CString tString;
			tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
			AppendLogMessages(tString);
			continue;
		}
	}
}


int CAzureKinect::StopAcquisitions()
{
	int nRet = 0;
	// Stop cameras and recordings (if any)
	size_t nKinects = m_kinects.size();
	for (int i = 0; i < nKinects; i++) {
		CString tString;
		tString.Format(L"Stop acquisition from Azure Kinect %d (S/N %s)\r\n", i, GetWChar(m_kinects[i]->SerialNumber.c_str()));
		AppendLogMessages(tString);
		try {
			m_kinects[i]->Device.Worker.stop_cameras();
			m_kinects[i]->Device.Worker.stop_imu();
			if (m_kinects[i]->Recorder.Worker) {
				m_kinects[i]->Recorder.Worker.flush();
				m_kinects[i]->Recorder.Worker.close();
			}
		}
		catch (const std::exception& e) {
			CString tString;
			tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
			AppendLogMessages(tString);
			nRet = 1;
			continue;
		}		
	}	
	return nRet;
}


int CAzureKinect::StopRecordings()
{
	int nRet = 0;
	// It stops the recordings but not the cameras
	size_t nKinects = m_kinects.size();
	for (int i = 0; i < nKinects; i++) {
		CString tString;
		tString.Format(L"Stop recording from Azure Kinect %d (S/N %s)\r\nCorresponding filename: %s\r\n", i, GetWChar(m_kinects[i]->SerialNumber.c_str()), GetWChar(m_kinects[i]->Recorder.Filename));
		AppendLogMessages(tString);
		try {
			if (m_kinects[i]->Recorder.Worker) {
				m_kinects[i]->Recorder.Worker.flush();
				m_kinects[i]->Recorder.Worker.close();
			}
		}
		catch (const std::exception& e) {
			CString tString;
			tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
			AppendLogMessages(tString);
			nRet = 1;
			continue;
		}
	}
	return nRet;
}


bool CAzureKinect::WaitForFirstCapture()
{
	// Wait for the first capture before starting
	size_t nKinects = m_kinects.size();
	for (int i = 0; i < nKinects; i++) {
		
		// Set the timeout depending on the camera mode
		int32_t timeout_sec_for_first_capture;
		if (m_kinects[i]->Device.Configuration.wired_sync_mode == K4A_WIRED_SYNC_MODE_SUBORDINATE)
			timeout_sec_for_first_capture = 360;
		else
			timeout_sec_for_first_capture = 60;

		// Set the start time
		int nRet = 0;
		clock_t startClock = clock();
		while ((clock() - startClock) < (CLOCKS_PER_SEC * timeout_sec_for_first_capture)) {
			MyCapture tCapture;
			tCapture.Capture = NULL;
			nRet = GetCapture(1, i, tCapture);
			if (nRet == 0)
				break;
			else if (nRet == -1)
				return false;
		}

		if (nRet == 1) {
			CString tString;
			tString.Format(L"Failed with exception:\r\nTimed out waiting for first capture\r\n");
			AppendLogMessages(tString);
			return false;
		}
	}
	return true;
}


int CAzureKinect::GetCapture(int step_or_pos, int idxDevice, MyCapture &myCapture)
{
	int nRet = 0;
	if (myCapture.Capture)
		myCapture.Capture = NULL;
	try {
		myCapture.ReferenceKinect = idxDevice;
		if (m_kinects[idxDevice]->Device.Worker) {
			for (int i = 0; i < step_or_pos; i++) {
				if (!m_kinects[idxDevice]->Device.Worker.get_capture(&myCapture.Capture, std::chrono::milliseconds(1000 / m_fps)))
					nRet = 1;
			}				
		}
		else if (m_kinects[idxDevice]->Player.Worker) {
			unsigned long long extended_pos = (unsigned long long)step_or_pos * 1000000ull;
			std::chrono::microseconds offset = std::chrono::microseconds(extended_pos / m_kinects[idxDevice]->Player.Fps);
			m_kinects[idxDevice]->Player.Worker.seek_timestamp(offset, K4A_PLAYBACK_SEEK_BEGIN);
			if (!m_kinects[idxDevice]->Player.Worker.get_next_capture(&myCapture.Capture))
				nRet = 1;
		}
	}
	catch (const std::exception& e)
	{
		CString tString;
		tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
		AppendLogMessages(tString);
		nRet = -1;
	}
	return nRet;
}


size_t CAzureKinect::GetCaptures(int step_or_pos)
{
	size_t nKinects = m_kinects.size();
	for (int i = 0; i < nKinects; i++) {
		MyCapture tCapture;
		tCapture.Capture = NULL;
		int nRet = GetCapture(step_or_pos, i, tCapture);
		if (nRet == 0) {
			m_captures.push_back((std::shared_ptr<MyCapture>) std::make_shared<MyCapture>(tCapture));
			if (m_kinects[i]->Recorder.Worker) {
				try {
					m_kinects[i]->Recorder.Worker.write_capture(tCapture.Capture);
				}
				catch (const std::exception& e)
				{
					CString tString;
					tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
					AppendLogMessages(tString);
					continue;
				}
			}
		}
		else {
			CString tString;
			tString.Format(L"Frame %d: Unavailable capture from virtual Azure Kinect %d (S/N %s)\r\n", step_or_pos, i, GetWChar(m_kinects[i]->SerialNumber.c_str()));
			AppendLogMessages(tString);
			continue;
		}					
	}
	return m_captures.size();
}


size_t CAzureKinect::GetIMUSamples(int step_or_pos)
{
	size_t nKinects = m_kinects.size();
	for (int i = 0; i < nKinects; i++) {
		try {
			MyImuSample tImuSample;
			tImuSample.SerialNumber = m_kinects[i]->SerialNumber;

			// Capture a imu sample
			if (m_kinects[i]->Device.Worker) {
				for (int i = 0; i < step_or_pos; i++) {
					if (!m_kinects[i]->Device.Worker.get_imu_sample(&tImuSample.Imu, std::chrono::milliseconds(1000 / m_fps)))
						continue;
				}
			}
			else if (m_kinects[i]->Player.Worker) {
				unsigned long long extended_pos = (unsigned long long)step_or_pos * 1000000ull;
				std::chrono::microseconds offset = std::chrono::microseconds(extended_pos / m_kinects[i]->Player.Fps);
				m_kinects[i]->Player.Worker.seek_timestamp(offset, K4A_PLAYBACK_SEEK_BEGIN);
				if (!m_kinects[i]->Player.Worker.get_next_imu_sample(&tImuSample.Imu)) {
					CString tString;
					tString.Format(L"Frame %d: Unavailable IMU sample from virtual Azure Kinect %d (S/N %s)\r\n", step_or_pos, i, GetWChar(m_kinects[i]->SerialNumber.c_str()));
					AppendLogMessages(tString);
					continue;
				}					
			}
				
			// Access the accelerometer readings
			if (tImuSample.Imu.temperature) {
				m_imuSamples.push_back(tImuSample);
				if (m_kinects[i]->Recorder.Worker && m_kinects[i]->Recorder.b_recordImu)
					m_kinects[i]->Recorder.Worker.write_imu_sample(tImuSample.Imu);
			}
		}
		catch (const std::exception& e)
		{
			CString tString;
			tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
			AppendLogMessages(tString);
			continue;
		}
	}

	return m_imuSamples.size();
}

void CAzureKinect::DoBodyTracking()
{
	size_t nCaptures = m_captures.size();
	for (int i = 0; i < nCaptures; i++) {
		MyBodyFrame tMyBodyFrame;
		try {
			// Do body tracking
			m_kinects[m_captures[i]->ReferenceKinect]->Tracker.enqueue_capture(m_captures[i]->Capture, std::chrono::milliseconds(K4A_WAIT_INFINITE));
			// Pop results
			tMyBodyFrame.BodyFrame = m_kinects[m_captures[i]->ReferenceKinect]->Tracker.pop_result(std::chrono::milliseconds(K4A_WAIT_INFINITE));
			tMyBodyFrame.ReferenceCapture = i;
			// Assign results
			m_bodyFrames.push_back((std::shared_ptr<MyBodyFrame>) std::make_shared<MyBodyFrame>(tMyBodyFrame));

			CString tString;
			tString.Format(L"%d user(s) found from Azure Kinect %d (S/N %s)\r\n", tMyBodyFrame.BodyFrame.get_num_bodies(), i, GetWChar(m_kinects[i]->SerialNumber.c_str()));
			AppendLogMessages(tString);
		}
		catch (const std::exception& e)
		{
			CString tString;
			tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
			AppendLogMessages(tString);
			continue;
		}
	}
}

bool CAzureKinect::GetIMU(int idxDevice, AK::SIMU & IMU)
{
	if (idxDevice < m_imuSamples.size()) {
		IMU.SerialNumber = m_imuSamples[idxDevice].SerialNumber;
		IMU.Temperature = m_imuSamples[idxDevice].Imu.temperature;
		IMU.Accelerometer.Timestamp = m_imuSamples[idxDevice].Imu.acc_timestamp_usec;
		IMU.Accelerometer.X = m_imuSamples[idxDevice].Imu.acc_sample.xyz.x;
		IMU.Accelerometer.Y = m_imuSamples[idxDevice].Imu.acc_sample.xyz.y;
		IMU.Accelerometer.Z = m_imuSamples[idxDevice].Imu.acc_sample.xyz.z;
		IMU.Gyroscope.Timestamp = m_imuSamples[idxDevice].Imu.gyro_timestamp_usec;
		IMU.Gyroscope.X = m_imuSamples[idxDevice].Imu.gyro_sample.xyz.x;
		IMU.Gyroscope.Y = m_imuSamples[idxDevice].Imu.gyro_sample.xyz.y;
		IMU.Gyroscope.Z = m_imuSamples[idxDevice].Imu.gyro_sample.xyz.z;
		return true;
	}
	else {
		IMU.SerialNumber = "Not Available\0";
		IMU.Temperature = 0.f;
		IMU.Accelerometer.Timestamp = 0;
		IMU.Accelerometer.X = 0.f;
		IMU.Accelerometer.Y = 0.f;
		IMU.Accelerometer.Z = 0.f;
		IMU.Gyroscope.Timestamp = 0;
		IMU.Gyroscope.X = 0.f;
		IMU.Gyroscope.Y = 0.f;
		IMU.Gyroscope.Z = 0.f;
		return false;
	}
}

int CAzureKinect::GetIMUs(std::vector<AK::SIMU> &IMUs)
{
	int nRet = 0;
	for (int i = 0; i < m_imuSamples.size(); i++) {
		AK::SIMU tImu;
		if (GetIMU(i, tImu))
			IMUs.push_back(tImu);
		else
			nRet = -1;
	}
	return nRet;
}

void CAzureKinect::ReleaseCaptures()
{
	if (m_captures.size()>0)
		m_captures.clear();
}

void CAzureKinect::ReleaseIMUSamples()
{
	if (m_imuSamples.size() > 0)
		m_imuSamples.clear();
}

void CAzureKinect::ReleaseBodyFrames()
{
	if (m_bodyFrames.size() > 0)
		m_bodyFrames.clear();
}

int CAzureKinect::GetImages(std::vector<AK::SImage>& Images, AK::EImageData ImageData)
{
	int nRet = 0;
	size_t nCaptures = m_captures.size();
	for (int i = 0; i < nCaptures; i++) {
		AK::SImage tImage;
		
		// Init tImage
		tImage.Data.assign(1,(uint8_t)255);
		tImage.ReferenceKinect = -1;
		tImage.Timestamp.Device = std::chrono::microseconds(0);
		tImage.Timestamp.System = std::chrono::nanoseconds(0);
		tImage.SerialNumber = m_kinects[m_captures[i]->ReferenceKinect]->SerialNumber;
		try {
			k4a::image k4aImage;
			// Get the proper data
			switch (ImageData) {
			case AK::COLOR:
				k4aImage = m_captures[i]->Capture.get_color_image();
				tImage.Timestamp.Device = k4aImage.get_device_timestamp();
				tImage.Timestamp.System = k4aImage.get_system_timestamp();
				break;

			case AK::COLORA:
			{
				// Recover color and depth images from the reference capture
				k4a::image k4aColor = m_captures[i]->Capture.get_color_image();
				k4a::image k4aDepth = m_captures[i]->Capture.get_depth_image();

				// Convert color image to BGRA32 
				cv::Mat cvColor;
				k4aImageToCvMat(k4aColor, CV_8UC3, true).copyTo(cvColor); // True because I need a CV_8UC4 output compatible with K4A_IMAGE_FORMAT_COLOR_BGRA32
				k4a::image k4aColorBGRA32;
				k4aColorBGRA32 = k4a::image::create_from_buffer(
					K4A_IMAGE_FORMAT_COLOR_BGRA32,
					cvColor.cols,
					cvColor.rows,
					(int)cvColor.step,
					cvColor.data,
					cvColor.step * cvColor.rows,
					nullptr,
					nullptr
				);

				// --- Transform color into depth camera geometry ---
				int DepthW = k4aDepth.get_width_pixels();
				int DepthH = k4aDepth.get_height_pixels();
				// Create image container
				k4aImage = k4a::image::create(K4A_IMAGE_FORMAT_COLOR_BGRA32, DepthW, DepthH, 4 * DepthW * (int)sizeof(uint8_t));

				// Align to color
				m_kinects[m_captures[i]->ReferenceKinect]->Transformation.color_image_to_depth_camera(k4aDepth, k4aColorBGRA32, &k4aImage);

				tImage.Timestamp.Device = k4aColor.get_device_timestamp();
				tImage.Timestamp.System = k4aColor.get_system_timestamp();
				break;
			}

			case AK::IR:
				k4aImage = m_captures[i]->Capture.get_ir_image();
				tImage.Timestamp.Device = k4aImage.get_device_timestamp();
				tImage.Timestamp.System = k4aImage.get_system_timestamp();
				break;

			case AK::IRA:
			{
				k4a::image k4aColor = m_captures[i]->Capture.get_color_image();
				k4a::image k4aDepth = m_captures[i]->Capture.get_depth_image();
				k4a::image k4aIr = m_captures[i]->Capture.get_ir_image();

				// Change Infrared data type from IR16 to CUSTOM16
				int IrW = k4aIr.get_width_pixels();
				int IrH = k4aIr.get_height_pixels();
				int IrS = k4aIr.get_stride_bytes();
				uint8_t* IrBuffer = k4aIr.get_buffer();
				k4a::image k4aIrCustom16 = k4a::image::create_from_buffer(K4A_IMAGE_FORMAT_CUSTOM16,
					IrW, IrH, IrS, IrBuffer, IrH * IrS, NULL, NULL);
					/*[](void* _buffer, void* context) {
						delete[](uint8_t*) _buffer;
						(void)context;
					},
					NULL);	*/

				// --- Transform IR into color camera geometry ---
				int ColorW = k4aColor.get_width_pixels();
				int ColorH = k4aColor.get_height_pixels();
				// Create image container
				k4aImage = k4a::image::create(K4A_IMAGE_FORMAT_CUSTOM16, ColorW, ColorH, ColorW * (int)sizeof(uint16_t));
				k4a::image k4aDepthTemp = k4a::image::create(K4A_IMAGE_FORMAT_DEPTH16, ColorW, ColorH, ColorW * (int)sizeof(uint16_t));

				// Align to color
				m_kinects[m_captures[i]->ReferenceKinect]->Transformation.depth_image_to_color_camera_custom(k4aDepth, k4aIrCustom16, &k4aDepthTemp, &k4aImage, K4A_TRANSFORMATION_INTERPOLATION_TYPE_NEAREST, 0);
				tImage.Timestamp.Device = k4aIr.get_device_timestamp();
				tImage.Timestamp.System = k4aIr.get_system_timestamp();

				break;
			}
				

			case AK::DEPTH:
				k4aImage = m_captures[i]->Capture.get_depth_image();
				tImage.Timestamp.Device = k4aImage.get_device_timestamp();
				tImage.Timestamp.System = k4aImage.get_system_timestamp();
				break;

			case AK::DEPTHA:
			{
				// Recover color and depth images from the reference capture
				k4a::image k4aColor = m_captures[i]->Capture.get_color_image();
				k4a::image k4aDepth = m_captures[i]->Capture.get_depth_image();

				// --- Transform depth and body index maps into color camera geometry ---
				int ColorW = k4aColor.get_width_pixels();
				int ColorH = k4aColor.get_height_pixels();
				// Create image container
				k4aImage = k4a::image::create(K4A_IMAGE_FORMAT_DEPTH16, ColorW, ColorH, ColorW * (int)sizeof(uint16_t));

				// Align to color
				m_kinects[m_captures[i]->ReferenceKinect]->Transformation.depth_image_to_color_camera(k4aDepth, &k4aImage);
				tImage.Timestamp.Device = k4aDepth.get_device_timestamp();
				tImage.Timestamp.System = k4aDepth.get_system_timestamp();
				break;
			}
				
			}

			// Set vector<uchar>
			const uint8_t* ImageBuffer = k4aImage.get_buffer();
			tImage.Data.assign(ImageBuffer, ImageBuffer + k4aImage.get_size());

			// Set height, width, timestamps and reference kinect
			tImage.Height = k4aImage.get_height_pixels();
			tImage.Width = k4aImage.get_width_pixels();
			tImage.ReferenceKinect = m_captures[i]->ReferenceKinect;

			Images.push_back(tImage);
			nRet = 0;
		}
		catch (const std::exception& e)
		{
			// If exception is thrown, fill the frame with SImages (can be empty)
			Images.push_back(tImage);
			CString tString;
			tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
			AppendLogMessages(tString);
			nRet = -1;
			continue;
		}
	}
	return nRet;
}

int CAzureKinect::GetBodyIndexMaps(std::vector<AK::SImage>& Images, AK::EBodyIndexData BodyIndexData)
{
	int nRet = 0;
	size_t nBodyFrames = m_bodyFrames.size();
	for (int i = 0; i < nBodyFrames; i++) {
		AK::SImage tImage;

		// Init tImage
		tImage.Data.assign(1, (uint8_t)255);
		tImage.ReferenceKinect = -1;
		tImage.Timestamp.Device = std::chrono::microseconds(0);
		tImage.Timestamp.System = std::chrono::nanoseconds(0);
		tImage.SerialNumber = m_kinects[m_captures[m_bodyFrames[i]->ReferenceCapture]->ReferenceKinect]->SerialNumber;
		try {
			// Read body index map
			k4a::image k4aImage;
			switch (BodyIndexData) {
			case AK::BODYINDEXMAP:
				k4aImage = m_bodyFrames[i]->BodyFrame.get_body_index_map();
				break;

			case AK::BODYINDEXMAPA:
				// Read body index map
				k4a::image k4aBodyIndexMap = m_bodyFrames[i]->BodyFrame.get_body_index_map();

				// Recover color and depth images from the reference capture
				k4a::image k4aColor = m_captures[m_bodyFrames[i]->ReferenceCapture]->Capture.get_color_image();
				k4a::image k4aDepth = m_captures[m_bodyFrames[i]->ReferenceCapture]->Capture.get_depth_image();

				// --- Transform depth and body index maps into color camera geometry ---
				int ColorW = k4aColor.get_width_pixels();
				int ColorH = k4aColor.get_height_pixels();
				// Create image container
				k4a::image k4aDepthA = k4a::image::create(K4A_IMAGE_FORMAT_DEPTH16, ColorW, ColorH, ColorW * (int)sizeof(uint16_t));
				k4aImage = k4a::image::create(K4A_IMAGE_FORMAT_CUSTOM8, ColorW, ColorH, ColorW * (int)sizeof(uint8_t));
				// Do the transformations
				m_kinects[m_captures[m_bodyFrames[i]->ReferenceCapture]->ReferenceKinect]->
					Transformation.depth_image_to_color_camera_custom(
						k4aDepth, k4aBodyIndexMap, &k4aDepthA, &k4aImage,
						K4A_TRANSFORMATION_INTERPOLATION_TYPE_NEAREST, K4ABT_BODY_INDEX_MAP_BACKGROUND);
				break;
			}
						
			// Set vector<uchar>
			const uint8_t* ImageBuffer = k4aImage.get_buffer();
			tImage.Data.assign(ImageBuffer, ImageBuffer + k4aImage.get_size());
			
			// Set height, width, timestamps and reference kinect
			tImage.Height = k4aImage.get_height_pixels();
			tImage.Width = k4aImage.get_width_pixels();
			tImage.Timestamp.Device = m_bodyFrames[i]->BodyFrame.get_device_timestamp();
			tImage.Timestamp.System = m_captures[m_bodyFrames[i]->ReferenceCapture]->Capture.get_depth_image().get_system_timestamp();
			tImage.ReferenceKinect = m_captures[m_bodyFrames[i]->ReferenceCapture]->ReferenceKinect;

			Images.push_back(tImage);
		}
		catch (const std::exception& e)
		{
			// If exception is thrown, fill the frame with SImages (can be empty)
			Images.push_back(tImage);
			CString tString;
			tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
			AppendLogMessages(tString);
			nRet = -1;
			continue;
		}
	}
	return nRet;
}


int CAzureKinect::GetBodyIDs(std::vector<AK::SBodyID>& BodyIDs)
{
	int nRet = 0;
	size_t nBodyFrames = m_bodyFrames.size();
	for (int i = 0; i < nBodyFrames; i++) {
		AK::SBodyID tBodyID;
		// Init SBodyID
		tBodyID.Timestamp.Device = std::chrono::microseconds(0);
		tBodyID.Timestamp.System = std::chrono::nanoseconds(0);
		tBodyID.SerialNumber = m_kinects[m_captures[m_bodyFrames[i]->ReferenceCapture]->ReferenceKinect]->SerialNumber;
		try {
			// Get the number of bodies
			uint32_t num_bodies = m_bodyFrames[i]->BodyFrame.get_num_bodies();

			// Read body index map
			k4a::image k4aBodyIndexMap = m_bodyFrames[i]->BodyFrame.get_body_index_map();
			int BIM_H = k4aBodyIndexMap.get_height_pixels();
			int BIM_W = k4aBodyIndexMap.get_width_pixels();
			uint8_t *pBIM = k4aBodyIndexMap.get_buffer();

			// Search Transform each 3d joints from 3d depth space to 2d color image space
			for (uint32_t j = 0; j < num_bodies; j++) {
				AK::SSkeleton tSkeleton;
				k4abt_skeleton_t k4aSkeleton = m_bodyFrames[i]->BodyFrame.get_body_skeleton(j);
				
				// Get bodyID info from the first joint with K4ABT_JOINT_CONFIDENCE_MEDIUM (detected, not predicted)
				std::vector<uint8_t> vBIM;
				for (int joint_id = 0; joint_id < (int)K4ABT_JOINT_COUNT; joint_id++) {
					if (k4aSkeleton.joints[joint_id].confidence_level == K4ABT_JOINT_CONFIDENCE_MEDIUM) {
						k4a_float2_t jointD2D;
						if (m_kinects[m_captures[m_bodyFrames[i]->ReferenceCapture]->ReferenceKinect]->
							Calibration.convert_3d_to_2d(k4aSkeleton.joints[joint_id].position, 
							K4A_CALIBRATION_TYPE_DEPTH, K4A_CALIBRATION_TYPE_DEPTH, &jointD2D)) {
								int bufPos = (int)jointD2D.xy.x + ((int)jointD2D.xy.y)*BIM_W;
								if (bufPos >= 0 && bufPos < BIM_W*BIM_H) {
									uint8_t bimValue = pBIM[bufPos];
									if (bimValue != K4ABT_BODY_INDEX_MAP_BACKGROUND)
										vBIM.push_back(bimValue);
								}								
						}						
					}
				}
				uint8_t bimValueOK;
				if (vBIM.size() > 0)
					bimValueOK = mode(vBIM);
				else
					bimValueOK = K4ABT_BODY_INDEX_MAP_BACKGROUND;
				tBodyID.BodyIndexToBodyID.emplace(bimValueOK, m_bodyFrames[i]->BodyFrame.get_body_id(bimValueOK));
			}
			tBodyID.Timestamp.Device = k4aBodyIndexMap.get_device_timestamp();
			tBodyID.Timestamp.System = m_captures[m_bodyFrames[i]->ReferenceCapture]->Capture.get_depth_image().get_system_timestamp();
			BodyIDs.push_back(tBodyID);
		}
		catch (const std::exception& e)
		{
			// If exception is thrown, fill the frame with SImages (can be empty)
			BodyIDs.push_back(tBodyID);
			CString tString;
			tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
			AppendLogMessages(tString);
			nRet = -1;
			continue;
		}
	}
	return nRet;
}

int CAzureKinect::GetSkeletons(std::vector<AK::SSkeleton> &Skeletons, bool isLight)
{
	int nRet = 0;
	size_t nBodyFrames = m_bodyFrames.size();
	for (int i = 0; i < nBodyFrames; i++) {
		// Get the number of bodies
		uint32_t num_bodies = m_bodyFrames[i]->BodyFrame.get_num_bodies();

		// Transform each 3d joints from 3d depth space to 2d color image space
		for (uint32_t j = 0; j < num_bodies; j++) {
			AK::SSkeleton tSkeleton;
			tSkeleton.ReferenceKinect = m_captures[m_bodyFrames[i]->ReferenceCapture]->ReferenceKinect;
			k4abt_skeleton_t k4aSkeleton = m_bodyFrames[i]->BodyFrame.get_body_skeleton(j);

			if (isLight) {
				if (AssignSkeletonsLight(k4aSkeleton, m_bodyFrames[i]->BodyFrame, m_kinects[m_captures[m_bodyFrames[i]->ReferenceCapture]->ReferenceKinect], tSkeleton))
					Skeletons.push_back(tSkeleton);
			}
			else {
				if (AssignSkeletons(k4aSkeleton, m_bodyFrames[i]->BodyFrame, m_kinects[m_captures[m_bodyFrames[i]->ReferenceCapture]->ReferenceKinect], tSkeleton))
					Skeletons.push_back(tSkeleton);
			}
		}
	}
	return nRet;
}

int CAzureKinect::GetPointcloudsOnColorGeometry(std::vector<AK::SPointcloud>& Clouds)
{
	int nRet = 0;
	size_t nCaptures = m_captures.size();
	for (int i = 0; i < nCaptures; i++) {
		AK::SPointcloud tCloud;

		// Init tImage
		tCloud.ReferenceKinect = -1;
		tCloud.Timestamp.Device = std::chrono::microseconds(0);
		tCloud.Timestamp.System = std::chrono::nanoseconds(0);
		tCloud.SerialNumber = m_kinects[m_captures[i]->ReferenceKinect]->SerialNumber;
		try {
			k4a::image k4aDepthA;

			// Recover color and depth images from the reference capture
			k4a::image k4aColor = m_captures[i]->Capture.get_color_image();
			k4a::image k4aDepth = m_captures[i]->Capture.get_depth_image();

			// --- Transform depth and body index maps into color camera geometry ---
			int ColorW = k4aColor.get_width_pixels();
			int ColorH = k4aColor.get_height_pixels();

			if (ColorW > 0) {
				// Create image and pointcloud containers
				k4aDepthA = k4a::image::create(K4A_IMAGE_FORMAT_DEPTH16, ColorW, ColorH, ColorW * (int)sizeof(uint16_t));
				k4a::image k4aPointcloud = NULL;
				k4aPointcloud = k4a::image::create(K4A_IMAGE_FORMAT_CUSTOM, ColorW, ColorH, ColorW * 3 * (int)sizeof(int16_t));

				// Align to color
				m_kinects[m_captures[i]->ReferenceKinect]->Transformation.depth_image_to_color_camera(k4aDepth, &k4aDepthA);
				m_kinects[m_captures[i]->ReferenceKinect]->Transformation.depth_image_to_point_cloud(k4aDepthA, K4A_CALIBRATION_TYPE_COLOR, &k4aPointcloud);

				int16_t* k4aPointcloudBuffer = (int16_t*)(void*)k4aPointcloud.get_buffer();
				
				cv::Mat tPointcloudMat, tColorMat;
				k4aImageToCvMat(k4aPointcloud, CV_16U).copyTo(tPointcloudMat);
				k4aImageToCvMat(k4aColor, CV_8UC3).copyTo(tColorMat);
				int index = -1;

				for (int row = 0; row < tColorMat.rows; row++) {
					for (int col = 0; col < tColorMat.cols; col++) {
						index++;
						AK::_PointXYZBGR tPoint;

						tPoint.x = k4aPointcloudBuffer[3 * index + 0] / 1000.0f;
						tPoint.y = k4aPointcloudBuffer[3 * index + 1] / 1000.0f;
						tPoint.z = k4aPointcloudBuffer[3 * index + 2] / 1000.0f;
						if (tPoint.z == 0)
							continue;

						tPoint.b = tColorMat.ptr<cv::Vec3b>(row)[col][0];
						tPoint.g = tColorMat.ptr<cv::Vec3b>(row)[col][1];
						tPoint.r = tColorMat.ptr<cv::Vec3b>(row)[col][2];
						if (tPoint.b == 0 && tPoint.g == 0 && tPoint.r == 0)
							continue;

						tCloud.Points.push_back(tPoint);
					}
				}

			}
			
			// Set timestamps and reference kinect
			tCloud.Timestamp.Device = k4aDepth.get_device_timestamp();
			tCloud.Timestamp.System = k4aDepth.get_system_timestamp();
			tCloud.ReferenceKinect = m_captures[i]->ReferenceKinect;

			Clouds.push_back(tCloud);
			nRet = 0;
		}
		catch (const std::exception& e)
		{
			// If exception is thrown, fill the frame with SImages (can be empty)
			Clouds.push_back(tCloud);
			CString tString;
			tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
			AppendLogMessages(tString);
			nRet = -1;
			continue;
		}
	}
	return nRet;
}


int CAzureKinect::GetPointcloudsOnDepthGeometry(std::vector<AK::SPointcloud>& Clouds)
{
	int nRet = 0;
	size_t nCaptures = m_captures.size();
	for (int i = 0; i < nCaptures; i++) {
		AK::SPointcloud tCloud;

		// Init tImage
		tCloud.ReferenceKinect = -1;
		tCloud.Timestamp.Device = std::chrono::microseconds(0);
		tCloud.Timestamp.System = std::chrono::nanoseconds(0);
		tCloud.SerialNumber = m_kinects[m_captures[i]->ReferenceKinect]->SerialNumber;
		try {
			k4a::image k4aColorA;

			// Recover color and depth images from the reference capture
			k4a::image k4aColor = m_captures[i]->Capture.get_color_image();
			k4a::image k4aDepth = m_captures[i]->Capture.get_depth_image();

			cv::Mat cvColor;
			k4aImageToCvMat(k4aColor, CV_8UC3, true).copyTo(cvColor); // True because I need a CV_8UC4 output compatible with K4A_IMAGE_FORMAT_COLOR_BGRA32
			k4a::image k4aColorBGRA32;
			k4aColorBGRA32 = k4a::image::create_from_buffer(
				K4A_IMAGE_FORMAT_COLOR_BGRA32,
				cvColor.cols,
				cvColor.rows,
				(int)cvColor.step,
				cvColor.data,
				cvColor.step * cvColor.rows,
				nullptr,
				nullptr
			);

			// --- Transform depth and body index maps into color camera geometry ---
			//int ColorW = k4aColor.get_width_pixels();
			//int ColorH = k4aColor.get_height_pixels();

			int DepthW = k4aDepth.get_width_pixels();
			int DepthH = k4aDepth.get_height_pixels();

			if (DepthW > 0) {
				// Create image and pointcloud containers
				k4aColorA = k4a::image::create(K4A_IMAGE_FORMAT_COLOR_BGRA32, DepthW, DepthH, DepthW * 4 * (int)sizeof(uint8_t));
				k4a::image k4aPointcloud = NULL;
				k4aPointcloud = k4a::image::create(K4A_IMAGE_FORMAT_CUSTOM, DepthW, DepthH, DepthW * 3 * (int)sizeof(int16_t));

				// Align color to depth
				m_kinects[m_captures[i]->ReferenceKinect]->Transformation.color_image_to_depth_camera(k4aDepth, k4aColorBGRA32, &k4aColorA);
				m_kinects[m_captures[i]->ReferenceKinect]->Transformation.depth_image_to_point_cloud(k4aDepth, K4A_CALIBRATION_TYPE_DEPTH, &k4aPointcloud);

				int16_t* k4aPointcloudBuffer = (int16_t*)(void*)k4aPointcloud.get_buffer();

				cv::Mat tPointcloudMat, tColorMat;
				k4aImageToCvMat(k4aPointcloud, CV_16U).copyTo(tPointcloudMat);
				k4aImageToCvMat(k4aColorA, CV_8UC4).copyTo(tColorMat);
				int index = -1;

				for (int row = 0; row < tColorMat.rows; row++) {
					for (int col = 0; col < tColorMat.cols; col++) {
						index++;
						AK::_PointXYZBGR tPoint;

						tPoint.x = k4aPointcloudBuffer[3 * index + 0] / 1000.0f;
						tPoint.y = k4aPointcloudBuffer[3 * index + 1] / 1000.0f;
						tPoint.z = k4aPointcloudBuffer[3 * index + 2] / 1000.0f;
						if (tPoint.z == 0)
							continue;

						tPoint.b = tColorMat.ptr<cv::Vec4b>(row)[col][0];
						tPoint.g = tColorMat.ptr<cv::Vec4b>(row)[col][1];
						tPoint.r = tColorMat.ptr<cv::Vec4b>(row)[col][2];
						if (tPoint.b == 0 && tPoint.g == 0 && tPoint.r == 0)
							continue;

						tCloud.Points.push_back(tPoint);
					}
				}

			}

			// Set timestamps and reference kinect
			tCloud.Timestamp.Device = k4aDepth.get_device_timestamp();
			tCloud.Timestamp.System = k4aDepth.get_system_timestamp();
			tCloud.ReferenceKinect = m_captures[i]->ReferenceKinect;

			Clouds.push_back(tCloud);
			nRet = 0;
		}
		catch (const std::exception& e)
		{
			// If exception is thrown, fill the frame with SImages (can be empty)
			Clouds.push_back(tCloud);
			CString tString;
			tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
			AppendLogMessages(tString);
			nRet = -1;
			continue;
		}
	}
	return nRet;
}


bool CAzureKinect::AssignSkeletons(k4abt_skeleton_t k4aSkeleton, k4abt::frame bodyFrame, std::shared_ptr<MyKinect> pKinect, AK::SSkeleton & skeleton)
{
	// Initializations
	for (int joint_id = 0; joint_id < (int)K4ABT_JOINT_COUNT; joint_id++) {
		skeleton.Confidence[joint_id] = 0.f;
		for (int p = 0; p < 3; p++)
			skeleton.Joint3D[joint_id].Position[p] = 0.f;
		for (int o = 0; o < 4; o++)
			skeleton.Joint3D[joint_id].Orientation[o] = 0.f;
		skeleton.Joint2DColorSpace[joint_id].x = 0.f;
		skeleton.Joint2DColorSpace[joint_id].y = 0.f;
		skeleton.Joint2DDepthSpace[joint_id].x = 0.f;
		skeleton.Joint2DDepthSpace[joint_id].y = 0.f;
		skeleton.Timestamp.Device = std::chrono::microseconds(0);
		skeleton.Timestamp.System = std::chrono::nanoseconds(0);
		skeleton.SerialNumber = "Not Available\0";
		skeleton.BodyID = 0;
	}

	bool isCalibOK = true;
	k4a::image BIM = bodyFrame.get_body_index_map();
	uint8_t *bim = BIM.get_buffer();
	std::vector<uint32_t> vBodyID;
	const int cols = BIM.get_width_pixels();
	const int rows = BIM.get_height_pixels();
		
	for (int joint_id = 0; joint_id < (int)K4ABT_JOINT_COUNT; joint_id++) {
		try {
			skeleton.Confidence[joint_id] = (float)k4aSkeleton.joints[joint_id].confidence_level / (float)(K4ABT_JOINT_CONFIDENCE_LEVELS_COUNT - 1);
			for (int p = 0; p < 3; p++)
				skeleton.Joint3D[joint_id].Position[p] = k4aSkeleton.joints[joint_id].position.v[p];
			for (int o = 0; o < 4; o++)
				skeleton.Joint3D[joint_id].Orientation[o] = k4aSkeleton.joints[joint_id].orientation.v[o];
			
			k4a_float2_t joint_in_color_2d;
			if (pKinect->Calibration.convert_3d_to_2d(k4aSkeleton.joints[joint_id].position,
				K4A_CALIBRATION_TYPE_DEPTH, K4A_CALIBRATION_TYPE_COLOR, &joint_in_color_2d)) {
				skeleton.Joint2DColorSpace[joint_id].x = joint_in_color_2d.xy.x;
				skeleton.Joint2DColorSpace[joint_id].y = joint_in_color_2d.xy.y;
			}

			k4a_float2_t joint_in_depth_2d;
			if (pKinect->Calibration.convert_3d_to_2d(k4aSkeleton.joints[joint_id].position,
				K4A_CALIBRATION_TYPE_DEPTH, K4A_CALIBRATION_TYPE_DEPTH, &joint_in_depth_2d)) {
				skeleton.Joint2DDepthSpace[joint_id].x = joint_in_depth_2d.xy.x;
				skeleton.Joint2DDepthSpace[joint_id].y = joint_in_depth_2d.xy.y;
			}

			// Compute the bodyID of the whole skeleton by considering only the information of the joints 
			// with the highest confidence (K4ABT_JOINT_CONFIDENCE_MEDIUM = visible joints in current sdk (v1.0))
			if (k4aSkeleton.joints[joint_id].confidence_level == K4ABT_JOINT_CONFIDENCE_MEDIUM) {
				int bufPos = (int)joint_in_depth_2d.xy.x + ((int)joint_in_depth_2d.xy.y)*cols;
				if (bufPos >= 0 && bufPos < cols*rows) {
					uint8_t bimValue = bim[bufPos];
					if (bimValue != K4ABT_BODY_INDEX_MAP_BACKGROUND)
						vBodyID.push_back(bodyFrame.get_body_id(bimValue));
				}
			}
			
		}
		catch (const std::exception& e)
		{
			CString tString;
			tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
			AppendLogMessages(tString);
			isCalibOK = false;
			continue;
		}
	}
	
	// Compute the mode of vBodyID to state the body ID
	if (vBodyID.size() > 0)
		skeleton.BodyID = mode(vBodyID);
	else
		skeleton.BodyID = bodyFrame.get_body_id(K4ABT_BODY_INDEX_MAP_BACKGROUND);
		
	skeleton.Timestamp.Device = bodyFrame.get_device_timestamp();
	skeleton.Timestamp.System = bodyFrame.get_capture().get_depth_image().get_system_timestamp();
	skeleton.SerialNumber = pKinect->SerialNumber;
	return isCalibOK;
}

bool CAzureKinect::AssignSkeletonsLight(k4abt_skeleton_t k4aSkeleton, k4abt::frame bodyFrame, std::shared_ptr<MyKinect> pKinect, AK::SSkeleton& skeleton)
{
	bool isAssignmentOK = true;

	// Initializations
	for (int joint_id = 0; joint_id < (int)K4ABT_JOINT_COUNT; joint_id++) {
		skeleton.Confidence[joint_id] = 0.f;
		for (int p = 0; p < 3; p++)
			skeleton.Joint3D[joint_id].Position[p] = 0.f;
		for (int o = 0; o < 4; o++)
			skeleton.Joint3D[joint_id].Orientation[o] = 0.f;
		skeleton.Joint2DColorSpace[joint_id].x = 0.f;
		skeleton.Joint2DColorSpace[joint_id].y = 0.f;
		skeleton.Joint2DDepthSpace[joint_id].x = 0.f;
		skeleton.Joint2DDepthSpace[joint_id].y = 0.f;
		skeleton.Timestamp.Device = std::chrono::microseconds(0);
		skeleton.Timestamp.System = std::chrono::nanoseconds(0);
		skeleton.SerialNumber = "Not Available\0";
		skeleton.BodyID = 0;
	}

	for (int joint_id = 0; joint_id < (int)K4ABT_JOINT_COUNT; joint_id++) {
		try {
			skeleton.Confidence[joint_id] = (float)k4aSkeleton.joints[joint_id].confidence_level / (float)(K4ABT_JOINT_CONFIDENCE_LEVELS_COUNT - 1);
			for (int p = 0; p < 3; p++)
				skeleton.Joint3D[joint_id].Position[p] = k4aSkeleton.joints[joint_id].position.v[p];
			for (int o = 0; o < 4; o++)
				skeleton.Joint3D[joint_id].Orientation[o] = k4aSkeleton.joints[joint_id].orientation.v[o];			
		}
		catch (const std::exception& e)
		{
			CString tString;
			tString.Format(L"Failed with exception:\r\n%s\r\n", GetWChar(e.what()));
			AppendLogMessages(tString);
			isAssignmentOK = false;
			continue;
		}
	}

	skeleton.Timestamp.Device = bodyFrame.get_device_timestamp();
	skeleton.Timestamp.System = bodyFrame.get_capture().get_depth_image().get_system_timestamp();
	skeleton.SerialNumber = pKinect->SerialNumber;
	return isAssignmentOK;
}


void CAzureKinect::AssignSkeletonsToParentKinect(std::vector<AK::SSkeleton> inSkeletons, std::vector<std::vector<AK::SSkeleton>>& perKinectSkeletons)
{
	//Initializations
	perKinectSkeletons.clear();
	std::vector<AK::SSkeleton> tSkeletons;
	size_t nBodyFrames = m_bodyFrames.size();
	perKinectSkeletons.reserve(nBodyFrames);
	for (int i = 0; i < nBodyFrames; i++) 
		perKinectSkeletons.push_back(tSkeletons);

	//Assignments
	for (int i = 0; i < inSkeletons.size(); i++) 
		perKinectSkeletons[inSkeletons[i].ReferenceKinect].push_back(inSkeletons[i]);

}


std::vector<int> CAzureKinect::GetPlayerDurations()
{
	std::vector<int> vRet;
	size_t nPlaybacks = m_kinects.size();

	for (size_t i = 0; i < nPlaybacks; i++)
		vRet.push_back(m_kinects[i]->Player.NFrames);
	
	return vRet;
}

void CAzureKinect::GetSerialNumbers(std::vector<std::string>& Serials)
{
	Serials.clear();
	size_t nPlaybacks = m_kinects.size();

	for (size_t i = 0; i < nPlaybacks; i++)
		Serials.push_back(m_kinects[i]->SerialNumber);

	return;
}

void CAzureKinect::GetValidKinects(std::vector<bool>& ValidKinects)
{
	ValidKinects.clear();
	size_t nPlaybacks = m_kinects.size();

	for (size_t i = 0; i < nPlaybacks; i++)
		ValidKinects.push_back(false);

	for (size_t i = 0; i < m_captures.size(); i++)
		ValidKinects[m_captures[i]->ReferenceKinect] = true;

	return;
}


cv::Mat CAzureKinect::k4aImageToCvMat(k4a::image image, int CV_TYPE, bool BGR2BGRA)
{
	// get raw buffer
	uint8_t* buffer = image.get_buffer();
	int rows = image.get_height_pixels();
	int cols = image.get_width_pixels();

	if (CV_TYPE == CV_8UC3) {
		// Create a Size(1, nSize) Mat object of 8-bit, single-byte elements
		cv::Mat rawMat(1, 4 * rows * cols, CV_8UC1, (void*)buffer);
		cv::Mat retMat = imdecode(rawMat, cv::IMREAD_COLOR);
		if (BGR2BGRA)
			cvtColor(retMat, retMat, cv::COLOR_BGR2BGRA);
		return retMat;
	}
	else {
		// convert the raw buffer to cv::Mat
		cv::Mat retMat(rows, cols, CV_TYPE, (void*)buffer, cv::Mat::AUTO_STEP);
		return retMat;
	}
}


const wchar_t * CAzureKinect::GetWChar(const char *c)
{
	const size_t cSize = strlen(c) + 1;
	wchar_t* outWChar = new wchar_t[cSize];
	size_t outSize;
	mbstowcs_s(&outSize, outWChar, cSize, c, cSize - 1);

	return outWChar;
}


void CAzureKinect::SetContainer(void* Container)
{
	p_Container = Container;
}


void CAzureKinect::AppendLogMessages(LPCTSTR Message)
{
	CAzureKinectDlg* pDlg = (CAzureKinectDlg*)p_Container;

	// get the initial text length
	int nLength = pDlg->m_editLog.GetWindowTextLength();
	// put the selection at the end of text
	pDlg->m_editLog.SetSel(nLength, nLength);
	// replace the selection
	pDlg->m_editLog.ReplaceSel(Message);
}

void CAzureKinect::EmptyLogMessages()
{
	CAzureKinectDlg* pDlg = (CAzureKinectDlg*)p_Container;
	pDlg->m_editLog.SetWindowText(L"");
}
