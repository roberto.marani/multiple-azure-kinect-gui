#pragma once

#include <thread>
#include <string>
#include <sstream>
#include <fstream>
#include <opencv2/opencv.hpp>
#include "AzureKinect.h"
#include <filesystem>

enum conversion_type {
	CONVERT_SKELETONS,
	CONVERT_PNG_COLOR,
	CONVERT_PNG_COLOR_ALIGNED,
	CONVERT_PNG_DEPTH,
	CONVERT_PNG_DEPTH_ALIGNED,
	CONVERT_PNG_IR,
	CONVERT_PNG_IR_ALIGNED,
	CONVERT_RAW_COLOR,
	CONVERT_RAW_COLOR_ALIGNED,
	CONVERT_RAW_DEPTH,
	CONVERT_RAW_DEPTH_ALIGNED,
	CONVERT_RAW_IR,
	CONVERT_RAW_IR_ALIGNED,
	CONVERT_POINTCLOUDSCOLOR,
	CONVERT_POINTCLOUDSDEPTH
};

class CConverter
{
public:
	CConverter();
	~CConverter();

protected:
	std::vector<AK::SSkeleton> skeletons;
	AK::SPointcloud cloud;
	AK::SImage image, supportImage;
	int referenceKinect;

	std::string destination;
	conversion_type conversionType;
	int frameID;
	std::string serial;

	std::thread worker;
	template <typename F> inline void start_worker(const F& f) { worker = std::thread(f); };

public:
	void SetConverter(conversion_type cType, std::string inDestination, int inRefKinect);
	void SetSkeletons(std::vector<AK::SSkeleton> inSkeletons);
	void SetPointcloud(AK::SPointcloud inCloud);
	void SetImage(AK::SImage inImage);
	void SetSupportImage(AK::SImage inSupportImage);
	void SetFrameID(int inFrameID) { frameID = inFrameID; };
	void SetSerialNumber(std::string inSerial) { serial = inSerial; };

	conversion_type GetConversionType() { return conversionType; };
	void GetSerialNumber(std::string& outSerial) { outSerial = serial; };

	void StartConversionThreads();
	void EndConversionThreads();

	void ConvertSkeletons();
	void ConvertPointclouds();
	void ConvertPng();
	void ConvertRaw();

	int GetReferenceKinect();

};

