#pragma once

#include "AzureKinect.h"
#include "Display.h"


// CController

class CController : public CWinThread
{
	DECLARE_DYNCREATE(CController)

public:
	CController();           // protected constructor used by dynamic creation
	virtual ~CController();

public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual BOOL Run();

protected:
	DECLARE_MESSAGE_MAP()

public:
	bool b_acqLoop, b_setExposuresNow, b_startRecording, b_stopRecording, b_isRecording, b_existThread, b_isInitThreadEnded, b_doBodyTracking, b_isDoBodyTrackingChanged, b_takeSnapshot;
	size_t nKinects;
	CAzureKinect Kinects;
	CDisplay Display;
	CString m_basePathAndName;
	int exposure;
};


