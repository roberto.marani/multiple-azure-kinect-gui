//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AzureKinectGUI.rc
//
#define IDC_BROWSE                      3
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_AZUREKINECTGUI_DIALOG       102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDD_SETPATHANDNAME              131
#define IDC_STARTCAPTURE                1000
#define IDC_STOPCAPTURE                 1001
#define IDC_COMBOCONFIG                 1002
#define IDC_EDITEXP                     1005
#define IDC_MANUALEXP                   1006
#define IDC_AUTOEXP                     1007
#define IDC_EDITIMU                     1008
#define IDC_STARTRECORD                 1009
#define IDC_STOPRECORD                  1010
#define IDC_FILENAME                    1011
#define IDC_SETREFERENCEFILE            1012
#define IDC_CLOSEPLAYER                 1013
#define IDC_STARTEXPORT                 1014
#define IDC_PLAYERFILENAME              1015
#define IDC_SLIDER                      1016
#define IDC_CHECKSKELETONS              1017
#define IDC_FILENAMEDLG                 1018
#define IDC_PROGRESSEXPORT              1018
#define IDC_FILEPATHDLG                 1019
#define IDC_STOPEXPORT                  1019
#define IDC_FRAMEINDEX                  1020
#define IDC_CHECKPOINTCLOUDCOLOR        1021
#define IDC_EDITLOG                     1022
#define IDC_LOCALMASTER                 1023
#define IDC_REMOTEMASTER                1024
#define IDC_STANDALONE                  1025
#define IDC_CHECKPOINTCLOUDDEPTH        1026
#define IDC_CHECKCOLOR                  1027
#define IDC_CHECKCOLORALIGNED           1028
#define IDC_CHECKDEPTHALIGNED           1029
#define IDC_CHECKIRALIGNED              1030
#define IDC_CHECKDOBODYTRACKING         1031
#define IDC_SNAPNAME                    1032
#define IDC_CHANGE                      1033
#define IDC_CHANGE2                     1034
#define IDC_TAKE                        1034
#define IDC_CHECKDEPTH                  1035
#define IDC_CHECKIR                     1036

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1021
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
